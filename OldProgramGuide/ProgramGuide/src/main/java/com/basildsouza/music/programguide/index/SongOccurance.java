package com.basildsouza.music.programguide.index;

import com.basildsouza.music.programguide.SongKey;

public class SongOccurance {
  private final SongKey songKey;
  private final int weight;
  
  private SongOccurance(Builder builder) {
    this.songKey = builder.songKey;
    this.weight = builder.getTotalWeight();
  }
  
  public Integer getWeight() {
    return weight;
  }
  
  public SongKey getSongKey() {
    return songKey;
  }

  @Override
  public String toString() {
    return "SongOccurance [songKey=" + songKey + ", weight=" + weight + "]";
  }

  public static class Builder {
    private final SongKey songKey;
    private int weight;
    private int multipleoccurances;
    
    private static final int SINGLE_OCCURANCE_WEIGHT = 10;
    private static final int MAX_MULTI_OCCURANCE_WEIGHT = 5;
    private static final int SINGLE_TITLE_WEIGHT = 15;

    public Builder(SongKey songKey) {
      this.songKey = songKey;
    }
    
    private int getTotalWeight(){
      return weight + multipleoccurances;
    }

    public SongOccurance.Builder setBodyOccurance(int numOccurances) {
      if(numOccurances == 0){
        return this;
      }
      this.weight += SINGLE_OCCURANCE_WEIGHT;
      this.multipleoccurances += Math.min(MAX_MULTI_OCCURANCE_WEIGHT, numOccurances-1);
      return this;
    }
    
    public SongOccurance build() {
      return new SongOccurance(this);
    }

    @Override
    public String toString() {
      return "Builder [songKey=" + songKey + ", weight=" + weight
          + ", multipleoccurances=" + multipleoccurances + "]";
    }

    public Builder setTitleOccurance(int titleCount) {
      if(titleCount == 0){
        return this;
      }
      this.weight += SINGLE_TITLE_WEIGHT;
      this.multipleoccurances += Math.min(MAX_MULTI_OCCURANCE_WEIGHT, titleCount-1);
      return this;
    }
 }
}
