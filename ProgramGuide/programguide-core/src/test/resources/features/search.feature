Feature: Indexing and Searching of Songs

    Scenario Outline: Search Types
        Given song indexed with key '1' and title '<title>' and body '<body>'
        When searching for Term: '<SearchTerm>'
        Then  '<results>' result should be found

        Examples:
            | title       | body        | SearchTerm  | results |
            | TERM1       | A Body | TERM1       | 1       |
            | TERM1 TERM2 | A Body | TERM1       | 1       |
            | TERM1 TERM2 | A Body | TERM1 TERM2 | 1       |
            | TERM1TERM2  | A Body | TERM1       | 0       |

            | The Title | TERM1       | TERM1       | 1       |
            | The Title | TERM1 TERM2 | TERM1       | 1       |
            | The Title | TERM1 TERM2 | TERM1 TERM2 | 1       |
            | The Title | TERM1TERM2  | TERM1       | 0       |

            | TERM1       | TERM2       | TERM1 TERM2 | 1       |

    Scenario: Remove Song
        Given song indexed with key '1' and title 'TERM1' and body 'A Body'
        And song indexed with key '2' and title 'TERM1' and body 'A Body'
        And song with key '1' is removed from index
        When searching for Term: 'TERM1'
        Then '1' result should be found

    Scenario: Edit Song
        Given song indexed with key '1' and title 'TERM1' and body 'TERM2'
        And song re-indexed with key '1' and title 'NEW_TERM1' and body 'NEW_TERM2'
        When searching for Term: 'TERM1 TERM2'
        Then '0' result should be found
        When searching for Term: 'NEW_TERM1  NEW_TERM2'
        Then '1' result should be found

    Scenario:  Find All Terms
        Given song indexed with key '1' and title 'TERM1' and body 'TERM2'
        And song indexed with key '2' and title 'TERM1' and body 'TERM3'
        When  searching for Term: 'TERM1 TERM2'
        Then '1' result should be found

    Scenario: Find nothing
        Given song indexed with key '1' and title 'TERM1' and body 'TERM2'
        When  searching for Term: 'TERM1 TERM2 TERM3'
        Then '0' result should be found
