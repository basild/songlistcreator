package com.basildsouza.music.programguide;

import java.util.List;

public class Playlist {
  List<SongKey> songKeys;
  
  public Playlist(List<SongKey> songKeys) {
    this.songKeys = songKeys;
  }
  
  public List<SongKey> getSongKeys() {
    return songKeys;
  }
}
