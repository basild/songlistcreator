package com.basildsouza.music.songpresentor.slideshow;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;

public class MultiSongSlideShowSplitter {
  private final PageNumberParser pageNumberParser;

  public MultiSongSlideShowSplitter(PageNumberParser pageNumberParser) {
    this.pageNumberParser = pageNumberParser;
  }

  public List<XMLSlideShow> splitPpts(XMLSlideShow ppt) {
    final List<XMLSlideShow> splitPpts = new ArrayList<XMLSlideShow>();
    
    XMLSlideShow splitPpt = new XMLSlideShow();
    for (XSLFSlide slide : ppt.getSlides()) {
      PageNumber pageNum = pageNumberParser.getPageNumber(slide);
      if(slide.getTitle() == null){
        System.out.println("Missing Title. Skipping: " + slide.toString());
        continue;
      }
      if (pageNum == null) {
        System.out.println("Missing Page Numbers. Skipping: " + slide.getTitle());
        continue;
      }
      splitPpt.createSlide().importContent(slide);
      if (pageNum.isLastPage()) {
        splitPpts.add(splitPpt);
        splitPpt = new XMLSlideShow();
      }
    }
    return splitPpts;
  }
}
