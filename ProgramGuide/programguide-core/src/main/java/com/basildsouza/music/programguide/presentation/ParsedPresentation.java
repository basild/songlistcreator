package com.basildsouza.music.programguide.presentation;

import lombok.Data;

import java.util.List;

@Data
public class ParsedPresentation {
  private final String title;
  private final List<PresentationPage> presentationPage;

  public String getAllLyrics() {
    StringBuilder sb = new StringBuilder();
    presentationPage.stream().map(PresentationPage::getBody).forEach(s -> sb.append(s).append('\n'));

    return sb.toString();
  }

}
