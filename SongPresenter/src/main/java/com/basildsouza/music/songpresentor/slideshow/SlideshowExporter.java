package com.basildsouza.music.songpresentor.slideshow;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;

import com.basildsouza.music.programguide.Playlist;
import com.basildsouza.music.programguide.SongKey;
import com.basildsouza.music.songpresentor.PlaylistExporter;

public class SlideshowExporter implements PlaylistExporter<XMLSlideShow>{
  private final SlideShowStore slideShowStore;
  private final SlideShowReader slideShowReader;
  private XMLSlideShow slideShowTemplate;
  
  public SlideshowExporter(SlideShowStore slideShowStore, 
                           SlideShowReader slideShowReader) {
    this.slideShowStore = slideShowStore;
    this.slideShowReader = slideShowReader;
  }
  
  public void setTemplate(File pptFile) throws FileNotFoundException, IOException{
    slideShowTemplate = slideShowReader.readSlide(pptFile);
  }

  public XMLSlideShow exportPlaylist(Playlist playlist) {
    List<SongKey> songKeys = playlist.getSongKeys();
    XMLSlideShow exportPpt = new XMLSlideShow();
    mergeSlideShow(exportPpt, slideShowTemplate);
    for(SongKey songKey : songKeys){
      XMLSlideShow slideShow = slideShowStore.getSlideShow(songKey);
      mergeSlideShow(exportPpt, slideShow);
    }
    
    return exportPpt;
  }

  private void mergeSlideShow(XMLSlideShow exportPpt, XMLSlideShow slideShow) {
    for (XSLFSlide slide : slideShow.getSlides()) {
      exportPpt.createSlide().importContent(slide);
    }
  }
}
