// Song
function displayViewPage(){
	var songID = getQueryVariable("songID");
	var songObj;
	if(songID == null || songID == ""){
		errorNotify("Could not find a song ID");
		songObj = {title: "Error Loading Title", 
				   lyrics: "Error Loading Lyrics", 
				   timing: "Err",
				   key: "Err"};
		populateViewPage(songObj);
	} else {
		fetchSong(songID, populateViewPage);
	}
}

function populateViewPage(songObj, status, jqXHR){
	$("#song-view-title").html(songObj.title);
	$("#song-view-key").html(songObj.key);
	$("#song-view-timing").html(songObj.timing);
	$("#song-view-rhtyhm").html(songObj.rhtyhm);
	$("#song-view-lyrics").html(songObj.lyrics);
	$("#song-view-refurl").attr("html", songObj.referenceURL);
}
// End Song

// Song Modify
function displayModifyPage(){
	var songID = getQueryVariable("songID");
	if(songID == null || !songID){
		return;
	}
	$("#song-edit-songID").attr("value", songID);
	fetchSong(songID, populateEditPage);
}
	
function populateEditPage(songObj){
	$("#song-add-title").attr("value", songObj.title);
	$("#song-add-key").attr("value", songObj.key);
	$("#song-add-timing").attr("value", songObj.timing);
	$("#song-add-rhythm").attr("value", songObj.rhythm);
	$("#song-add-lyrics").attr("value", songObj.lyrics);
	$("#song-add-refurl").attr("value", songObj.referenceURL);
}
// End Song Modify

// Song Index
function deleteSongFromDB(songID, songTitle){
	$("#song-options-popup").popup("close");
	confirmNotification("Do you want to delete: " + songTitle, 
			function(){
				//errorNotify("Deleting from DB");
				$.ajax({
				    url: '/rest/song/'+songID,
				    type: 'DELETE',
				    cache: false,
				    success: function(result) {
				        // Do something with the result
				        infoNotify("Deleted Song: " + songTitle);
				        setTimeout(function() {
				        	fetchSongArray();
			            }, 5000);
				    }
				});
			}, function (){
				//infoNotify("No action taken.");
			});
}

function createGuide(){
	var baseURL = "guide.html?songs=";
	var optionList = $('#selected-songs option');
	console.log(optionList.length);
	if(optionList.length < 2){
		// Notify: No songs selected
		return;
	}
	optionList.each(function (index){
		if(this.value == ""){
			return;
		}
		
		baseURL += this.value + ",";
	});
	baseURL = baseURL.substring(0, baseURL.length-1);
	window.open(baseURL, '_blank');
}

function removeSelectedFromSongGuide(){
	var selectedSongs = $('#selected-songs option:selected');
	selectedSongs.remove();
	
	$('#selected-songs').selectmenu("refresh", "true");
}

function addToSongGuide(songID, songTitle){
	$("#song-options-popup").popup("close");
	
	if($("#selected-songs option[value='" + songID + "']").length > 0){
		// Already Exists. 
		errorNotify("Already in list: " + songTitle);
		return;
	}
	
	console.log("Adding to guide list: " + songID + ", " + songTitle);
	
	var mySelect = $("#selected-songs");
    mySelect.append($('<option></option>').val(songID).html(songTitle).attr("selected", "true"));
    mySelect.selectmenu("refresh", "true");
	infoNotify("Added: " + songTitle);
}

function showSongOptions(songID, title){
	$("#song-options-menu-header").text(title);
	$("#song-options-menu-view").attr("href", "song.html?songID="+songID);
	$("#song-options-menu-edit").attr("href", "song-modify.html?songID="+songID);
	$("#song-options-menu-delete").attr("onclick", "deleteSongFromDB("+songID +", '" + title + "')"); // Should be converted to a function
	$("#song-options-menu-add-guide").attr("onclick", "addToSongGuide(" + songID + ", '" + title +"')"); 
	$("#song-options-popup").popup("open");
}

function updateSongList(data){
	var listHeader = $( "#song-index-placeholder" )
	
	listHeader.empty();
	var count = 0;
	while(data[count]){
		var currSong = data[count];
		if(currSong.rhythm == null){
			currSong.rhythm = "";
		}
		listHeader.append(
			'<li>' +
				'<a href="#" onclick="showSongOptions(' + currSong.songID + ',\'' + currSong.title + '\')"><h3>' + currSong.title + '</h3><p>' + 
				currSong.key + " - T=" + currSong.timing + " - R=" + currSong.rhythm +
				'</p></a>' +
				' ' +
				' <a href="#add-to-list-pass-param-here" data-role="button" data-icon="plus" onclick="addToSongGuide(' + currSong.songID + ', \'' + currSong.title +'\')">Add To List</a>' +
			'</li>');
		count++;
	}
	listHeader.listview().listview('refresh');
	//.trigger( "updatelayout");
	//$( "#song-index-placeholder" )
}

function fetchSongIndexArray(){
	var fullPath = '/rest/song/page/0';
	console.log(fullPath);
	$.ajax({
	  	url: fullPath,
	  	cache: false,
	  	success: updateSongList,
	  	dataType: 'json'
	});
	
}

// End Song Index



// Common Functions Across Entire App 
function getQueryVariable(varName)	{
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		if(pair[0] == varName){return pair[1];}
	}
	
	return (false);
}

function getSongListFromURL(){
	var csvSongs = getQueryVariable('songs');
	return "s="+ csvSongs.replace(/,/g, "&s=");
}

function fetchSong(songID, callback){
	var fullPath = '/rest/song/' + songID;
	console.log(fullPath);
	$.ajax({
		  url: fullPath,
		  success: callback,
		  dataType: 'json',
		  error: function (jqXHR, textStatus, errorThrown){
			  errorNotify("Could not fetch song: " + errorThrown);
		  }
		});
}


function fetchSongArray(){
	var queryString = getSongListFromURL();
	var fullPath = '/rest/song/list/?' + queryString;
	console.log(fullPath);
	$.ajax({
		  url: fullPath,
		  success: updateSongList,
		  dataType: 'json'
		});
	
}

//Not completed yet
function submitForm(formID, forwadingURL){
	
	$(formID).submit(function(event) {
		var form = $(formID);
		$.ajax({
			type: form.attr('method'),
			url: form.attr('action'),
			data: form.serialize()
		}).done(function() {
			$.mobile.changePage(forwardingURL);
		}).fail(function() {
			errorNotify("Could not change song");
		});
		
		event.preventDefault(); // Prevent the form from submitting via the browser.
	});	
}
// End common functions across entire app

// Generic Notification Function
function infoNotify(text){
	showNotification(text, 'success');
}

function errorNotify(text){
	showNotification(text, 'error');
}

function showNotification(notificationText, notificationType){
	var n = noty({layout: 'bottomCenter', text: notificationText, timeout: 5000, type: notificationType});
}

function confirmNotification(confirmText, okCallback, cancelCallback){
	noty({layout: 'center', text: confirmText, 
				buttons: [
				          {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
				              // this = button element
				              // $noty = $noty element
				              $noty.close();
							  okCallback();
				              //noty({text: 'You clicked "Ok" button', type: 'success'});
				            }
				          },
				          {addClass: 'btn btn-danger', text: 'Cancel', onClick: function($noty) {
				              $noty.close();
				        	  cancelCallback();
				              //noty({text: 'You clicked "Cancel" button', type: 'error'});
				            }
				          }
				        ]});
}
// End Generic Notification Function

// Generic Debug Functions
function registerLoggingEventListener(pageID, eventType){
	console.log("Registering event: " + pageID + " " + eventType);
	$(pageID).live(eventType, pageID, function(){
		console.log(pageID + " " + eventType);
	} );		
}

var pageID = "#test-page-1";
function registerAllEvents(pageID){
	console.log("Registering all events");
	registerLoggingEventListener(pageID, 'pageshow');
	//registerLoggingEventListener(pageID, 'pageload');
	//registerLoggingEventListener(pageID, 'pagecreate');
	//registerLoggingEventListener(pageID, 'pagecontainerload');
	//registerLoggingEventListener(pageID, 'pagecontainerloadfailed');
	//registerLoggingEventListener(pageID, 'pagebeforeshow');
	//registerLoggingEventListener(pageID, 'pagehide');
	//registerLoggingEventListener(pageID, 'pagecontainershow');
	
}
// End Generic Debug Function


