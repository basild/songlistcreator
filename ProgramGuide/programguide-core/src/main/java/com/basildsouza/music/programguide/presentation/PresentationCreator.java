package com.basildsouza.music.programguide.presentation;

import java.util.List;

public interface PresentationCreator {
  byte[] create(List<String> presentationKey);
}
