package com.basildsouza.music.programguide;

public interface SongCatalog extends SongProvider {
  SongKey addSong(Song song);

  void removeSong(SongKey songKey);
}
