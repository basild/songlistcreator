package com.basildsouza.music.songpresentor.index;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import com.basildsouza.music.programguide.Song;
import com.basildsouza.music.programguide.SongEntry;
import com.basildsouza.music.programguide.SongKey;
import com.basildsouza.music.programguide.index.CommonWordExcluder;
import com.basildsouza.music.programguide.index.LyricsTokenizer;
import com.basildsouza.music.programguide.index.SongIndex;
import com.basildsouza.music.programguide.index.SongIndexer;
import com.basildsouza.music.programguide.index.SongKeyGenerator;

public class DefaultSongIndexer implements SongIndexer{
  private final SongKeyGenerator songKeyGenerator;
  private final LyricsTokenizer lyricsTokenizer;
  private final CommonWordExcluder excluder;
  private final SongIndex songIndexStore;
  
  public DefaultSongIndexer(SongKeyGenerator songKeyGenerator, 
                             LyricsTokenizer lyricsTokenizer, 
                             CommonWordExcluder excluder,
                             SongIndex songIndex) {
    
    this.songKeyGenerator = songKeyGenerator;
    this.lyricsTokenizer = lyricsTokenizer;
    this.excluder = excluder;
    this.songIndexStore = songIndex;
  }
  
  public SongEntry addSong(Song song) {
    final Map<String, AtomicInteger> wordOccurances = countWordOccurances(song); 
    SongKey songKey = songKeyGenerator.getNextKey();
    for(Entry<String, AtomicInteger> wordEntry : wordOccurances.entrySet()){
      String searchTerm = wordEntry.getKey();
      int titleCount = song.getTitle().toUpperCase().split("\\b" + searchTerm + "\\b", -1).length-1;
      int bodyCount = wordEntry.getValue().get();
      songIndexStore.addIndexEntry(searchTerm, songKey,
                                   titleCount, bodyCount);
    }
    
    return new SongEntry(song, songKey);
  }

  private Map<String, AtomicInteger> countWordOccurances(Song song) {
    List<String> searchableTerms = getSearchableTerms(song);
    Map<String, AtomicInteger> wordOccurances = countOccurances(searchableTerms);
    return wordOccurances;
  }

  private List<String> getSearchableTerms(Song song) {
    List<String> individualWords = lyricsTokenizer.tokenize(song.getBody());
    individualWords.addAll(lyricsTokenizer.tokenize(song.getTitle()));
    return excluder.removeCommonWords(individualWords);
  }

  private Map<String, AtomicInteger> countOccurances(List<String> individualWords) {
    Map<String, AtomicInteger> wordOccurances = new HashMap<String, AtomicInteger>();
    for(String individualWord : individualWords){
      AtomicInteger wordCount = wordOccurances.get(individualWord);
      if(wordCount == null){
        wordCount = new AtomicInteger();
        wordOccurances.put(individualWord, wordCount);
      }
      wordCount.incrementAndGet();
    }
    return wordOccurances;
  }

  public void removeSong(SongEntry songEntry) {
    List<String> individualWords = getSearchableTerms(songEntry.getSong());
    Set<String> uniqueWords = new HashSet<String>(individualWords);
    SongKey songKey = songEntry.getSongKey();
    
    songIndexStore.removeIndexEntry(songKey, uniqueWords);
  }
}
