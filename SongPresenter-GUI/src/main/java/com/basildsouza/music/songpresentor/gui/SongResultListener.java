package com.basildsouza.music.songpresentor.gui;

import javax.swing.JButton;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

class SongResultListener implements ListDataListener {
  private final SongResultListModel songResultListModel;
  private final JButton btnAddSong;

  public SongResultListener(SongResultListModel songResultListModel,
      JButton btnAddSong) {
    this.songResultListModel = songResultListModel;
    this.btnAddSong = btnAddSong;
  }

  public void intervalRemoved(ListDataEvent e) {
    checkSizes();
  }
  
  public void intervalAdded(ListDataEvent e) {
    checkSizes();
  }
  
  public void contentsChanged(ListDataEvent e) {
    checkSizes();
  }
  
  private void checkSizes(){
    boolean searchResultsEmpty = songResultListModel.getSize() == 0;
    btnAddSong.setEnabled(!searchResultsEmpty);
  }
}