package com.basildsouza.music.programguide.search;

import java.util.stream.Stream;

public interface LyricsTokenizer {
  Stream<String> tokenize(String text);
}
