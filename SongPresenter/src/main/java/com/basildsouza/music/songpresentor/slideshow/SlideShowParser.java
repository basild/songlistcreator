package com.basildsouza.music.songpresentor.slideshow;

import java.util.List;

import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFAutoShape;
import org.apache.poi.xslf.usermodel.XSLFShape;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFTextShape;

import com.basildsouza.music.programguide.Song;

public class SlideShowParser {
  public Song parseSlide(XMLSlideShow slideShow) {
    List<XSLFSlide> slides = slideShow.getSlides();
    String fullSong = "";
    for (XSLFSlide slide : slides) {
      for (XSLFShape shape : slide.getShapes()) {
        if (shape instanceof XSLFAutoShape) {
          String partLyrics = ((XSLFTextShape) shape).getText();
          if (partLyrics.contains("\n")) {
            fullSong += partLyrics + "\n";
          }
        }
      }
    }
    return new Song(slideShow.getSlides().get(0).getTitle(), "", "", fullSong);
  }
}
