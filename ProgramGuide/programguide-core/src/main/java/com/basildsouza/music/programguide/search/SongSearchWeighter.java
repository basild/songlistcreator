package com.basildsouza.music.programguide.search;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@ToString
@EqualsAndHashCode(of = "songKey")
public class SongSearchWeighter {
  @Getter
  private final String songKey;
  private int weight;
  private int multipleOccurrences;

  private static final int SINGLE_OCCURRENCE_WEIGHT = 10;
  private static final int MAX_MULTI_OCCURRENCE_WEIGHT = 5;
  private static final int SINGLE_TITLE_WEIGHT = 15;

  public int getTotalWeight() {
    return weight + multipleOccurrences;
  }

  public SongSearchWeighter weighOverallOccurrence(int numOccurances) {
    if (numOccurances == 0) {
      return this;
    }
    this.weight += SINGLE_OCCURRENCE_WEIGHT;
    this.multipleOccurrences += Math.min(MAX_MULTI_OCCURRENCE_WEIGHT, numOccurances - 1);
    return this;
  }

  public SongSearchWeighter weighTitleOccurrence(int titleCount) {
    if (titleCount == 0) {
      return this;
    }
    this.weight += SINGLE_TITLE_WEIGHT;
    this.multipleOccurrences += Math.min(MAX_MULTI_OCCURRENCE_WEIGHT, titleCount - 1);
    return this;
  }


}
