package com.basildsouza.music.songpresentor.index;

import java.util.concurrent.atomic.AtomicInteger;

import com.basildsouza.music.programguide.SongKey;
import com.basildsouza.music.programguide.index.SongKeyGenerator;

public class InMemorySongKeyGenerator implements SongKeyGenerator{
  private final AtomicInteger counter = new AtomicInteger(0);

  public SongKey getNextKey() {
    return new SongKey(counter.getAndIncrement());
  }
}
