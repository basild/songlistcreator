package com.basildsouza.music.programguide.search;

import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.stereotype.Repository;

import java.util.List;

@Profile("Persisted")
@Repository
@RepositoryDefinition(domainClass = SongIndexEntry.class, idClass = SongIndexEntry.class)
public interface SongIndexRepository {
  SongIndexEntry save(SongIndexEntry songIndexEntry);

  List<SongIndexEntry> findBySearchToken(String searchToken);

  Iterable<SongIndexEntry> findAllById(Iterable<SongIndexEntryKey> indexEntryKeys);

  List<SongIndexEntry> findBySongKey(String songKey);

  void deleteBySongKey(String songKey);

  void deleteAll();
}