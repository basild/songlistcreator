package com.basildsouza.music.programguide;

public class Song {
  private final String title;
  private final String key;
  private final String timing;
  private final String body;

  public Song(String title, String key, String timing, String body) {
    this.title = title;
    this.key = key;
    this.timing = timing;
    this.body = body;
  }
  
  public String getKey() {
    return key;
  }
  
  public String getTiming() {
    return timing;
  }
  
  public String getTitle() {
    return title;
  }

  public String getBody() {
    return body;
  }

  @Override
  public String toString() {
    return "Song [title=" + title + ", key=" + key + ", timing=" + timing
        + ", body=" + body + "]";
  }
}
