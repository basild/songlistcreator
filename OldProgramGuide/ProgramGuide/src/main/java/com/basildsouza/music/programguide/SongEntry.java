package com.basildsouza.music.programguide;

public class SongEntry {
  private final SongKey songKey;
  private final Song song;

  public SongEntry(Song song, SongKey songKey) {
    this.song = song;
    this.songKey = songKey;
  }

  public SongKey getSongKey() {
    return songKey;
  }

  public Song getSong() {
    return song;
  }
}
