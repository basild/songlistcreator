package com.basildsouza.music.programguide.dal.jdoimpl;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import javax.jdo.annotations.PersistenceAware;

import com.basildsouza.music.programguide.dal.SongDAO;
import com.basildsouza.music.programguide.dal.entity.Song;

@PersistenceAware
public class GAESongDAOImpl implements SongDAO{

	@Override
	public Song getSong(Long songID) {
		PersistenceManager pm = PMFSingleton.getInstance().getPersistenceManager();
		pm.getFetchPlan().addGroup("lyrics-text");
		try {
			final Song song = pm.getObjectById(Song.class, songID);
			return pm.detachCopy(song);
		} finally {
			pm.close();
		}
	}

	@Override
	public List<Song> searchSongByTitle(String titleRegExp) {
		return null;
	}
//		PersistenceManager pm = PMFSingleton.getInstance().getPersistenceManager();
//		try {
//    		Query q = pm.newQuery(Song.class);
//    		q.setFilter("title == songIdToDel");
//    		q.declareParameters("Integer songIdToDel");
//    		q.execute(titleRegExp);
//		} finally {
//			pm.close();
//		}
//	}

	
	@Override
	public List<Song> searchSongByTitleAndContent(String searchRegExp) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Song> searchSongByTag(String tagName) {
		// TODO Auto-generated method stub
		return null;
	}
	
	// Very prone to race conditions, wont support multiple additions
	private Long getMaxSongID(){
		PersistenceManager pm = PMFSingleton.newPersistenceManager();
    	try{
    		Query query = pm.newQuery(Song.class);
    		query.setResult("max(this.songID)");
    		query.setResultClass(Long.class);
    		Long maxVal = (Long) query.execute();
    		System.out.println(maxVal);
    		if(maxVal == null){
    			return (long) 1;
    		}
			return maxVal+1;
    	} finally {
    		pm.close();
    	}
	}

	@Override
	public Song addSong(Song song) {
		PersistenceManager pm = PMFSingleton.newPersistenceManager();
		pm.setDetachAllOnCommit(true);
		pm.getFetchPlan().addGroup("lyrics-text");
		Transaction tx = pm.currentTransaction();
    	try{
    		if(!tx.isActive()) tx.begin();
    		if(song.getSongID() == null){ 
    			song.setSongID(getMaxSongID());
    		}
    		song = pm.makePersistent(song);
    		tx.commit();
    		return song;
    	} finally {
    		if(tx.isActive()){
    			tx.rollback();
    		}
    		pm.close();
    	}
	}

	@Override
	public void deleteSong(Long songID) {
    	PersistenceManager pm = PMFSingleton.newPersistenceManager();
    	Transaction tx = pm.currentTransaction();

    	try{
    		if(!tx.isActive()) tx.begin();
    		
    		javax.jdo.Query q = pm.newQuery(Song.class);
    		q.setFilter("songID == songIdToDel");
    		q.declareParameters("Integer songIdToDel");
    		q.deletePersistentAll(songID);
    		tx.commit();
    	} finally {
    		if(tx.isActive()){
    			tx.rollback();
    		}
    		pm.close();
    	}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Song> listAllSongs(int pageNumber, int pageSize) {
    	PersistenceManager pm = PMFSingleton.newPersistenceManager();
    	pm.getFetchPlan().addGroup("lyrics-text");
    	try{
    		Query query = pm.newQuery(Song.class);
    		query.setRange(pageNumber*pageSize, (pageNumber+1)*pageSize);
    		return new ArrayList<>(pm.detachCopyAll((List<Song>) query.execute()));
    		//TODO: this might not be a good option to create it all the time?
//    		DatastoreService datastoreService = DatastoreServiceFactory.getDatastoreService();
//    		Query q = new Query(Song.class);
//    		datastoreService.prepare(query)
//    		q.deletePersistentAll(songID);
    	} finally {
    		pm.close();
    	}		
	}

	@Override
	public List<Song> listSongsByID(List<Long> songIDs) {
		final List<Song> songList = new ArrayList<>(songIDs.size());
		for(Long songID : songIDs){
			songList.add(getSong(songID));
		}
		
		return songList;
	}
}
