package com.basildsouza.music.programguide.dal.jdoimpl;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;

public class PMFSingleton {
	private static final PersistenceManagerFactory instance 
		= JDOHelper.getPersistenceManagerFactory("transactions-optional");
        
    private PMFSingleton() {}

    public static PersistenceManagerFactory getInstance() {
        return instance;
    }
    
    public static PersistenceManager newPersistenceManager(){
    	return getInstance().getPersistenceManager();
    }
}
