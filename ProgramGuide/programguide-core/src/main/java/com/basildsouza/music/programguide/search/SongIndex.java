package com.basildsouza.music.programguide.search;

import java.util.Map;
import java.util.Set;

public interface SongIndex {
  void addEntry(String searchToken, String songKey,
                int titleCount, int totalCount);

  Map<String, SearchTokenOccuranceCount> find(String searchToken);

  Map<String, SearchTokenOccuranceCount> refineFind(String searchToken, Set<String> songKeys);

  Map<String, SearchTokenOccuranceCount> findBySongKey(String songKey);

  void removeEntries(String songKey);
}
