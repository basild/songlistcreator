//function deleteSongFromDB(songID, songTitle){
//	$("#song-options-popup").popup("close");
//	confirmNotification("Do you want to delete: " + songTitle, 
//			function(){
//				//errorNotify("Deleting from DB");
//				$.ajax({
//				    url: '/rest/song/'+songID,
//				    type: 'DELETE',
//				    cache: false,
//				    success: function(result) {
//				        // Do something with the result
//				        infoNotify("Deleted Song: " + songTitle);
//				        setTimeout(function() {
//				        	fetchSongArray();
//			            }, 5000);
//				    }
//				});
//			}, function (){
//				//infoNotify("No action taken.");
//			});
//}
//
//function createGuide(){
//	var baseURL = "guide.html?songs=";
//	var optionList = $('#selected-songs option');
//	console.log(optionList.length);
//	if(optionList.length < 2){
//		// Notify: No songs selected
//		return;
//	}
//	optionList.each(function (index){
//		if(this.value == ""){
//			return;
//		}
//		
//		baseURL += this.value + ",";
//	});
//	baseURL = baseURL.substring(0, baseURL.length-1);
//	window.open(baseURL, '_blank');
//}
//
//function removeSelectedFromSongGuide(){
//	var selectedSongs = $('#selected-songs option:selected');
//	selectedSongs.remove();
//	
//	$('#selected-songs').selectmenu("refresh", "true");
//}
//
//function addToSongGuide(songID, songTitle){
//	$("#song-options-popup").popup("close");
//	
//	if($("#selected-songs option[value='" + songID + "']").length > 0){
//		// Already Exists. 
//		errorNotify("Already in list: " + songTitle);
//		return;
//	}
//	
//	console.log("Adding to guide list: " + songID + ", " + songTitle);
//	
//	var mySelect = $("#selected-songs");
//    mySelect.append($('<option></option>').val(songID).html(songTitle).attr("selected", "true"));
//    mySelect.selectmenu("refresh", "true");
//	infoNotify("Added: " + songTitle);
//}
//
//function showSongOptions(songID, title){
//	$("#song-options-menu-header").text(title);
//	$("#song-options-menu-view").attr("href", "song.html?songID="+songID);
//	$("#song-options-menu-edit").attr("href", "song-modify.html?songID="+songID);
//	$("#song-options-menu-delete").attr("onclick", "deleteSongFromDB("+songID +", '" + title + "')"); // Should be converted to a function
//	$("#song-options-menu-add-guide").attr("onclick", "addToSongGuide(" + songID + ", '" + title +"')"); 
//	$("#song-options-popup").popup("open");
//}
//
//function updateSongList(data){
//	var listHeader = $( "#song-index-placeholder" )
//	
//	listHeader.empty();
//	var count = 0;
//	while(data[count]){
//		var currSong = data[count];
//		if(currSong.rhythm == null){
//			currSong.rhythm = "";
//		}
//		listHeader.append(
//			'<li>' +
//				'<a href="#" onclick="showSongOptions(' + currSong.songID + ',\'' + currSong.title + '\')"><h3>' + currSong.title + '</h3><p>' + 
//				currSong.key + " - T=" + currSong.timing + " - R=" + currSong.rhythm +
//				'</p></a>' +
//				' ' +
//				' <a href="#add-to-list-pass-param-here" data-role="button" data-icon="plus" onclick="addToSongGuide(' + currSong.songID + ', \'' + currSong.title +'\')">Add To List</a>' +
//			'</li>');
//		count++;
//	}
//	listHeader.listview().listview('refresh');
//	//.trigger( "updatelayout");
//	//$( "#song-index-placeholder" )
//}
//
//function fetchSongArray(){
//	var fullPath = '/rest/song/page/0';
//	console.log(fullPath);
//	$.ajax({
//	  	url: fullPath,
//	  	cache: false,
//	  	success: updateSongList,
//	  	dataType: 'json'
//	});
//	
//}
