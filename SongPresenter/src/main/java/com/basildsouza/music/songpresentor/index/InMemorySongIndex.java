package com.basildsouza.music.songpresentor.index;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.basildsouza.music.programguide.SongKey;
import com.basildsouza.music.programguide.index.SongIndex;
import com.basildsouza.music.programguide.index.SongIndexEntry;

public class InMemorySongIndex implements SongIndex{
  private final Map<String, Map<SongKey, SongIndexEntry>> occurances = new HashMap<String, Map<SongKey, SongIndexEntry>>();

  public void addIndexEntry(String searchTerm, SongKey songKey, 
                            int titleCount, int bodyCount) {
    Map<SongKey, SongIndexEntry> songOccurances = occurances.get(searchTerm);
    if(songOccurances == null){
      songOccurances = new HashMap<SongKey, SongIndexEntry>();
      occurances.put(searchTerm, songOccurances);
    }
    songOccurances.put(songKey, new SongIndexEntry(titleCount, bodyCount));
  }

  public Map<SongKey, SongIndexEntry> findIndexEntry(String searchToken) {
    Map<SongKey, SongIndexEntry> currentOccurances = occurances.get(searchToken);
    if(currentOccurances == null){
      return Collections.emptyMap();
    }
    return currentOccurances;
  }

  public void removeIndexEntry(SongKey songKey, Set<String> uniqueWords) {
    for(String word : uniqueWords){
      Map<SongKey, SongIndexEntry> songOccurances = occurances.get(word);
      if(songOccurances == null){
        continue;
      }
      songOccurances.remove(songKey);
    }
  }
}
