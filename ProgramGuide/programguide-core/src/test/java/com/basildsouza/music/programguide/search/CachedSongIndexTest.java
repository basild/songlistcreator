package com.basildsouza.music.programguide.search;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.internal.util.collections.Sets;

import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class CachedSongIndexTest {
  private SongIndex spyFallbackIndex;
  private InMemorySongIndex backingSongIndex;

  private CachedSongIndex systemUnderTest;

  private static final String DEFAULT_SONG_KEY = "DEFAULT_SONG_KEY";
  private static final String ANOTHER_SONG_KEY = "AnotherKey";
  private static final String DEFAULT_SEARCH_TOKEN = "DEFAULT_SEARCH_TOKEN";

  private static final int DEFAULT_TITLE_COUNT = 1;
  private static final int DEFAULT_TOTAL_COUNT = 1;

  @Before
  public void setup(){
    backingSongIndex = new InMemorySongIndex();
    spyFallbackIndex = Mockito.spy(backingSongIndex);
    systemUnderTest = new CachedSongIndex(spyFallbackIndex);
  }

  @Test
  public void addSong_reachesFallBackStore(){
    givenStoreWithDefaultTermAdded();

    verify(spyFallbackIndex).addEntry(DEFAULT_SEARCH_TOKEN, DEFAULT_SONG_KEY, DEFAULT_TITLE_COUNT, DEFAULT_TOTAL_COUNT);
  }

  @Test
  public void findCachedEntry_noFallbackQuery(){
    givenStoreWithDefaultTermAdded();
    reset(spyFallbackIndex);

    Map<String, SearchTokenOccuranceCount> searchResult = systemUnderTest.find(DEFAULT_SEARCH_TOKEN);

    verifySingleResultFound(searchResult);
    verifyFallbackStoreNotCalled();
  }

  @Test
  public void findBySongKey_fetchFromStore(){
    systemUnderTest.findBySongKey(DEFAULT_SONG_KEY);

    verify(spyFallbackIndex).findBySongKey(DEFAULT_SONG_KEY);
  }

  private void verifyFallbackStoreNotCalled() {
    verify(spyFallbackIndex, never()).find(DEFAULT_SEARCH_TOKEN);
  }

  private void givenStoreWithDefaultTermAdded() {
    systemUnderTest.addEntry(DEFAULT_SEARCH_TOKEN, DEFAULT_SONG_KEY, DEFAULT_TITLE_COUNT, DEFAULT_TOTAL_COUNT);
  }

  @Test
  public void removeSong_searchTokenRefresh(){
    givenStoreWithTwoSongs();

    systemUnderTest.removeEntries(DEFAULT_SONG_KEY);

    Map<String, SearchTokenOccuranceCount> searchResults = systemUnderTest.find(DEFAULT_SEARCH_TOKEN);

    verifySingleResultFound(searchResults);
    assertThat(searchResults.get(ANOTHER_SONG_KEY), notNullValue());
    verify(spyFallbackIndex).removeEntries(DEFAULT_SONG_KEY);
  }

  @Test
  public void findCacheMiss_shouldRefreshForTerm(){
    backingSongIndex.addEntry(DEFAULT_SEARCH_TOKEN, DEFAULT_SONG_KEY, DEFAULT_TITLE_COUNT, DEFAULT_TOTAL_COUNT);

    Map<String, SearchTokenOccuranceCount> searchResult = systemUnderTest.find(DEFAULT_SEARCH_TOKEN);

    verifySingleResultFound(searchResult);
  }

  @Test
  public void refineFind_worksOnCacheMiss(){
    backingSongIndex.addEntry(DEFAULT_SEARCH_TOKEN, DEFAULT_SONG_KEY, DEFAULT_TITLE_COUNT, DEFAULT_TOTAL_COUNT);
    backingSongIndex.addEntry(DEFAULT_SEARCH_TOKEN, ANOTHER_SONG_KEY, DEFAULT_TITLE_COUNT, DEFAULT_TOTAL_COUNT);

    Map<String, SearchTokenOccuranceCount> searchResult = systemUnderTest.refineFind(DEFAULT_SEARCH_TOKEN, Sets.newSet(DEFAULT_SONG_KEY));

    verifySingleResultFound(searchResult);
  }

  @Test
  public void clear_clearLocal(){
    givenStoreWithDefaultTermAdded();
    reset(spyFallbackIndex);

    systemUnderTest.clear();

    systemUnderTest.find(DEFAULT_SEARCH_TOKEN);

    verify(spyFallbackIndex).find(DEFAULT_SEARCH_TOKEN);
  }

  private void verifySingleResultFound(Map<String, SearchTokenOccuranceCount> searchResult) {
    assertThat(searchResult.size(), is(1));
  }


  private void givenStoreWithTwoSongs() {
    givenStoreWithDefaultTermAdded();
    systemUnderTest.addEntry("ANOTHER_SEARCH", DEFAULT_SONG_KEY, DEFAULT_TITLE_COUNT, DEFAULT_TOTAL_COUNT);
    systemUnderTest.addEntry(DEFAULT_SEARCH_TOKEN, ANOTHER_SONG_KEY, DEFAULT_TITLE_COUNT, DEFAULT_TOTAL_COUNT);
  }


}