package com.basildsouza.music.songpresentor.gui;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileFilter;

import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.basildsouza.music.programguide.Playlist;
import com.basildsouza.music.programguide.SongKey;
import com.basildsouza.music.programguide.index.LyricsTokenizer;
import com.basildsouza.music.programguide.index.SearchEntry;
import com.basildsouza.music.programguide.index.SongOccurance;
import com.basildsouza.music.programguide.index.SongSearcher;
import com.basildsouza.music.songpresentor.slideshow.MultiSongSlideShowImporter;
import com.basildsouza.music.songpresentor.slideshow.SlideShowStore;
import com.basildsouza.music.songpresentor.slideshow.SlideshowExporter;

public class SlideshowCreatorApplication extends JFrame {
  private static final long serialVersionUID = 1L;
  
  private static final String BASE;
  private static final String DATA;
  private static final File DEFAULT_INPUT_FILE;
  private static final String OUTPUT;

  private static SlideshowExporter playlistExporter;
  private static MultiSongSlideShowImporter slideShowImporter;
  private static SongSearcher songSearcher;
  private static SlideShowStore slideShowStore;
  private static LyricsTokenizer lyricsTokenizer;
  private JPanel contentPane;
  private JTextField songSearchTextField;
  private JTable playlistTable;
  private JList<SongResultEntry> searchResultList;
  
  private SongResultListModel songResultListModel = new SongResultListModel();
  private PlaylistTableModel playlistTableModel = new PlaylistTableModel();
  private JButton btnAddSong;
  private JButton btnExport;
  
  static {
    BASE = ".";
    DATA = BASE + "\\data";
    DEFAULT_INPUT_FILE = new File(DATA, "multi-song-input.pptx");
    OUTPUT = DATA + "\\output";

    System.setProperty("template.dir", DATA);
    System.setProperty("playlist.powerpoint.template", "template.pptx");
}

  public static void main(String[] args) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
    
    UIManager.setLookAndFeel(
        UIManager.getSystemLookAndFeelClassName());
    ApplicationContext context = new ClassPathXmlApplicationContext("/core.context.xml");
    playlistExporter = context.getBean("playlistExporter", SlideshowExporter.class);
    slideShowImporter = context.getBean("multiSongSlideShowImporter", MultiSongSlideShowImporter.class);
    songSearcher = context.getBean("songSearcher", SongSearcher.class);
    slideShowStore = context.getBean("slideShowStore", SlideShowStore.class);
    lyricsTokenizer = context.getBean("lyricsTokenizer", LyricsTokenizer.class);
    
    File inputFile = getInputFile();
    
    slideShowImporter.importSlides(inputFile);
    
    
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          SlideshowCreatorApplication frame = new SlideshowCreatorApplication();
          frame.setVisible(true);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  private static File getInputFile() {
    if(System.getProperty("song.presentor.use.default", "false").equalsIgnoreCase("true")){
      return DEFAULT_INPUT_FILE;
    }
    JFileChooser fileChooser = new JFileChooser(DEFAULT_INPUT_FILE);
    fileChooser.setFileFilter(createPptFilter());
    int openAction = fileChooser.showOpenDialog(null);
    File inputFile = openAction != JFileChooser.APPROVE_OPTION?DEFAULT_INPUT_FILE:fileChooser.getSelectedFile();
    return inputFile;
  }

  private static FileFilter createPptFilter() {
    return new javax.swing.filechooser.FileFilter() {
      
      @Override
      public String getDescription() {
        return "Power Point Presentations";
      }
      
      @Override
      public boolean accept(File f) {
        String name = f.getName().toLowerCase();
        return name.endsWith(".pptx") || name.endsWith(".ppt");
      }
    };
  }

  /**
   * Create the frame.
   */
  public SlideshowCreatorApplication() {
    setTitle("Slide Show Creator");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(100, 100, 640, 500);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    
    JPanel addSongPanel = new JPanel();
    addSongPanel.setBorder(new TitledBorder(null, "Add Song", TitledBorder.LEADING, TitledBorder.TOP, null, null));
    
    JPanel playlistPanel = new JPanel();
    playlistPanel.setBorder(new TitledBorder(null, "Playlist", TitledBorder.LEADING, TitledBorder.TOP, null, null));
    
    JButton btnExit = new JButton("Exit");
    btnExit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        System.exit(0);
      }
    });
    
    btnExport = new JButton("Export");
    btnExport.setEnabled(false);
    btnExport.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        JFileChooser fileChooser = new JFileChooser(new File(OUTPUT));
        fileChooser.setFileFilter(createPptFilter());
        int dialogReturnCode = fileChooser.showSaveDialog(SlideshowCreatorApplication.this);
        if(dialogReturnCode != JFileChooser.APPROVE_OPTION){
          return;
        }
        
        File exportFile = fileChooser.getSelectedFile();
        exportPlaylist(exportFile);
      }
    });
    
    JButton btnReset = new JButton("Reset");
    btnReset.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        playlistTableModel.clearAllPlaylistEntries();
      }
    });
    
    btnReset.setEnabled(false);
    GroupLayout gl_contentPane = new GroupLayout(contentPane);
    gl_contentPane.setHorizontalGroup(
      gl_contentPane.createParallelGroup(Alignment.TRAILING)
        .addGroup(gl_contentPane.createSequentialGroup()
          .addContainerGap(399, Short.MAX_VALUE)
          .addComponent(btnReset)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addComponent(btnExport, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addComponent(btnExit, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
          .addContainerGap())
        .addGroup(gl_contentPane.createSequentialGroup()
          .addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
            .addComponent(playlistPanel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 613, Short.MAX_VALUE)
            .addComponent(addSongPanel, GroupLayout.DEFAULT_SIZE, 622, Short.MAX_VALUE))
          .addGap(1))
    );
    gl_contentPane.setVerticalGroup(
      gl_contentPane.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_contentPane.createSequentialGroup()
          .addComponent(addSongPanel, GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(playlistPanel, GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)
          .addGap(18)
          .addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
            .addComponent(btnExit)
            .addComponent(btnExport)
            .addComponent(btnReset))
          .addGap(5))
    );
    gl_contentPane.linkSize(SwingConstants.HORIZONTAL, new Component[] {btnExit, btnExport, btnReset});
    
    JButton btnMoveUp = new JButton("Move up");
    btnMoveUp.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        int selectedRow = playlistTable.getSelectedRow();
        if(selectedRow < 1){
          return;
        }
        playlistTableModel.movePlaylistEntryUp(selectedRow);
      }
    });
    btnMoveUp.setEnabled(false);
    
    JButton btnMoveDown = new JButton("Move Down");
    btnMoveDown.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        int selectedRow = playlistTable.getSelectedRow();
        if(selectedRow < 0 || selectedRow >= playlistTableModel.getRowCount()){
          return;
        }
        playlistTableModel.movePlaylistEntryDown(selectedRow);
      }
    });
    btnMoveDown.setEnabled(false);
    
    JButton btnDelete = new JButton("Remove");
    btnDelete.setEnabled(false);
    btnDelete.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        int selectedRow = playlistTable.getSelectedRow();
        if(selectedRow < 0){
          return;
        }
        playlistTableModel.removePlaylistEntry(selectedRow);
      }
    });
    
    JScrollPane playlistScrollPane = new JScrollPane();
    GroupLayout gl_playlistPanel = new GroupLayout(playlistPanel);
    gl_playlistPanel.setHorizontalGroup(
      gl_playlistPanel.createParallelGroup(Alignment.TRAILING)
        .addGroup(gl_playlistPanel.createSequentialGroup()
          .addContainerGap()
          .addGroup(gl_playlistPanel.createParallelGroup(Alignment.TRAILING)
            .addComponent(playlistScrollPane, GroupLayout.DEFAULT_SIZE, 582, Short.MAX_VALUE)
            .addGroup(gl_playlistPanel.createSequentialGroup()
              .addComponent(btnDelete)
              .addPreferredGap(ComponentPlacement.RELATED)
              .addComponent(btnMoveDown)
              .addPreferredGap(ComponentPlacement.RELATED)
              .addComponent(btnMoveUp)))
          .addContainerGap())
    );
    gl_playlistPanel.setVerticalGroup(
      gl_playlistPanel.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_playlistPanel.createSequentialGroup()
          .addContainerGap()
          .addComponent(playlistScrollPane, GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addGroup(gl_playlistPanel.createParallelGroup(Alignment.BASELINE)
            .addComponent(btnMoveUp)
            .addComponent(btnMoveDown)
            .addComponent(btnDelete))
          .addContainerGap())
    );
    gl_playlistPanel.linkSize(SwingConstants.HORIZONTAL, new Component[] {btnMoveUp, btnMoveDown, btnDelete});
    
    playlistTable = new JTable();
    playlistScrollPane.setViewportView(playlistTable);
    playlistTable.setFillsViewportHeight(true);
    playlistTable.setModel(playlistTableModel);
    playlistTableModel.addTableModelListener(new PlaylistTableListener(playlistTableModel, btnExport, btnMoveDown, btnMoveUp, btnReset, btnDelete));
    playlistTable.getColumnModel().getColumn(0).setPreferredWidth(329);
    playlistTable.getColumnModel().getColumn(1).setResizable(false);
    playlistTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    playlistPanel.setLayout(gl_playlistPanel);
    
    songSearchTextField = new JTextField();
    songSearchTextField.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        addSelectedSongToPlaylist();
      }
    });
    
    songSearchTextField.addKeyListener(new KeyAdapter() {
      @Override
      public void keyTyped(KeyEvent keyEvent) {
        List<SongOccurance> findSong = songSearcher.findSong(createSearchEntry(songSearchTextField.getText() + keyEvent.getKeyChar()));
        List<SongResultEntry> results = new ArrayList<SongResultEntry>();
        for(SongOccurance songOccurance : findSong){
          results.add(new SongResultEntry(songOccurance.getSongKey(), slideShowStore.getSongEntry(songOccurance.getSongKey()).getSong().getTitle()));
        }
        searchResultList.clearSelection();
        songResultListModel.addAllEntries(results);
        
        if(!results.isEmpty()){
          searchResultList.setSelectedIndex(0);
        }
      }
    });
    songSearchTextField.setColumns(10);
    
    btnAddSong = new JButton("Add Song");
    btnAddSong.setEnabled(false);
    btnAddSong.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        addSelectedSongToPlaylist();
      }
    });
    songResultListModel.addListDataListener(new SongResultListener(songResultListModel, btnAddSong));
    
    JScrollPane scrollPane = new JScrollPane();
    GroupLayout gl_addSongPanel = new GroupLayout(addSongPanel);
    gl_addSongPanel.setHorizontalGroup(
      gl_addSongPanel.createParallelGroup(Alignment.TRAILING)
        .addGroup(Alignment.LEADING, gl_addSongPanel.createSequentialGroup()
          .addContainerGap()
          .addGroup(gl_addSongPanel.createParallelGroup(Alignment.LEADING)
            .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 581, Short.MAX_VALUE)
            .addGroup(gl_addSongPanel.createSequentialGroup()
              .addComponent(songSearchTextField, GroupLayout.DEFAULT_SIZE, 494, Short.MAX_VALUE)
              .addPreferredGap(ComponentPlacement.UNRELATED)
              .addComponent(btnAddSong)))
          .addContainerGap())
    );
    gl_addSongPanel.setVerticalGroup(
      gl_addSongPanel.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_addSongPanel.createSequentialGroup()
          .addContainerGap()
          .addGroup(gl_addSongPanel.createParallelGroup(Alignment.BASELINE)
            .addComponent(songSearchTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addComponent(btnAddSong))
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 99, Short.MAX_VALUE)
          .addGap(4))
    );
    
    searchResultList = new JList<SongResultEntry>();
    searchResultList.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent arg0) {
        if(arg0.getButton() != MouseEvent.BUTTON1 || 
           arg0.getClickCount() < 2){
          return;
        }
        addSelectedSongToPlaylist();
      }
    });
    scrollPane.setViewportView(searchResultList);
    searchResultList.setBorder(new TitledBorder(null, "Search Results", TitledBorder.LEADING, TitledBorder.TOP, null, null));
    searchResultList.setModel(songResultListModel);
    addSongPanel.setLayout(gl_addSongPanel);
    contentPane.setLayout(gl_contentPane);
  }

  private void addSelectedSongToPlaylist() {
    int selectedIndex = searchResultList.getSelectedIndex();
    if(selectedIndex < 0){
      return;
    }
    SongKey songKey = songResultListModel.getSongAt(selectedIndex);
    playlistTableModel.addPlaylistEntry(new PlaylistEntryRow(songKey, songResultListModel.getElementAt(selectedIndex).getTitle(), 
        slideShowStore.getSlideShow(songKey).getSlides().size()));
  }
  
  //TODO: These should be moved to clases
  private void savePresentation(XMLSlideShow splitPpt, File file) throws FileNotFoundException, IOException{
    System.out.println("Saving: " + file.getName() + " " + splitPpt.getSlides().size());
    splitPpt.write(new FileOutputStream(file));
  }

  private SearchEntry createSearchEntry(String fullSearch){
    return new SearchEntry(fullSearch, lyricsTokenizer.tokenize(fullSearch));
  }
  
  private void exportPlaylist(File exportFile) {
    Playlist playlist = playlistTableModel.getPlaylist();
    XMLSlideShow playlistSlideShow = playlistExporter.exportPlaylist(playlist);
    try {
      savePresentation(playlistSlideShow, exportFile);
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    JOptionPane.showMessageDialog(this, "Saved slide show to : " + exportFile.getAbsolutePath(), 
                                  "Saved SlideShow to: " + exportFile.getName(), 
                                  JOptionPane.INFORMATION_MESSAGE);
    
  }
  


}