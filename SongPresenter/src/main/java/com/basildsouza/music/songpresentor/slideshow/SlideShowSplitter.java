package com.basildsouza.music.songpresentor.slideshow;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.xslf.usermodel.XMLSlideShow;

public class SlideShowSplitter {

  public static void main(String args[]) throws FileNotFoundException,
      IOException {
    XMLSlideShow ppt = new XMLSlideShow(
        new FileInputStream( "F:\\Projects\\Java\\Slideshow Splitter\\SongPresenter\\conf\\multi-song-input.pptx"));
    
    MultiSongSlideShowSplitter slideShowSplitter = new MultiSongSlideShowSplitter(new PageNumberParser());
    List<XMLSlideShow> splitPpts = slideShowSplitter.splitPpts(ppt);
    
    for(XMLSlideShow slideShow : splitPpts){
      savePresentation(slideShow);
    }
  }


  private static void savePresentation(XMLSlideShow splitPpt)
      throws FileNotFoundException, IOException {
    String title = splitPpt.getSlides().get(0).getTitle();
    savePresentation(splitPpt, title);
  }
  
  private static void savePresentation(XMLSlideShow splitPpt, String title) throws FileNotFoundException, IOException{
    System.out.println("Saving: " + title + " " + splitPpt.getSlides().size());
    File outDir = new File("F:\\Projects\\Java\\Slideshow Splitter\\SongPresenter\\backup");
    File outFile = new File(outDir, title + ".pptx");
    splitPpt.write(new FileOutputStream(outFile));
  }
}