package com.basildsouza.music.programguide.search;


import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;

@Entity
@IdClass(SongIndexEntryKey.class)
@AllArgsConstructor
@Getter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class SongIndexEntry implements Serializable {
  @Id
  private String searchToken;
  private String songKey;
  @Embedded
  private SearchTokenOccuranceCount occurrenceCount;
}
