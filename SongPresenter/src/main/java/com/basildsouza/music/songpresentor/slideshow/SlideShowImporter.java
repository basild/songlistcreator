package com.basildsouza.music.songpresentor.slideshow;

import java.io.File;
import java.io.IOException;

public interface SlideShowImporter {
  public void importSlides(File pptDir) throws IOException;
}