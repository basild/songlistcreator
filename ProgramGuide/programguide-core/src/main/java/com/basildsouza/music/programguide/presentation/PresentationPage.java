package com.basildsouza.music.programguide.presentation;

import lombok.Data;

@Data
public class PresentationPage {
  private final int page;
  private final int totalPages;
  private final String body;
}
