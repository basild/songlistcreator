package com.basildsouza.music.programguide.search;

public interface CommonWordExcluder {
  boolean isExcluded(String word);

  boolean isIncluded(String word);
}
