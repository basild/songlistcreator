package com.basildsouza.music.programguide;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class WhatsappBot {
  private static Robot robot;

  private static String[] messages =
    {
      "First Message",
      "Another Message",
      "Third Message"
    };

  private static String whatTimeToSend = "13:03";
  private static int minsToSleep = 2;

  public static void main(String[] args) throws AWTException, InterruptedException {
    System.out.println("Sending whats app messages");

    LocalTime time = LocalTime.parse(whatTimeToSend, DateTimeFormatter.ofPattern("HH:mm"));
    LocalDateTime whenToSend = LocalDateTime.of(LocalDate.now(), time);
    long msToSleep = ChronoUnit.MILLIS.between(LocalDateTime.now(), whenToSend);
    if (msToSleep < 0) {
      System.out.println(whatTimeToSend + " has already passed. Exiting");
      return;
    }

    System.out.println("Sleeping until: " + whatTimeToSend);
    robot = new Robot();
    robot.setAutoDelay(25);

    Thread.sleep(msToSleep);
    for (String message : messages) {
      System.out.println("Sent Message: " + message);
      sendMessage(message);
      Thread.sleep(minsToSleep * 1000);
    }
  }

  private static void sendMessage(String message) {

    for (char c : message.toCharArray()) {
      if (Character.isUpperCase(c)) {
        robot.keyPress(KeyEvent.VK_SHIFT);
      }
      robot.keyPress(Character.toUpperCase(c));
      robot.keyRelease(Character.toUpperCase(c));

      if (Character.isUpperCase(c)) {
        robot.keyRelease(KeyEvent.VK_SHIFT);
      }
    }
    keyTyped(KeyEvent.VK_ENTER);

  }

  private static void keyTyped(int keyCode) {
    robot.keyPress(keyCode);

    robot.keyRelease(keyCode);
  }
}
