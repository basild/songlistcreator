package com.basildsouza.music.songpresentor.slideshow;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.xslf.usermodel.XSLFShape;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFTextBox;

public class PageNumberParser {
  public static final Pattern PAGE_PATTERN = Pattern
      .compile("(\\d+).*/.*(\\d+)");

  public PageNumber getPageNumber(XSLFSlide slide) {
    PageNumber pageNum = null;
    for (XSLFShape shape : slide.getShapes()) {
      if (shape.getClass() == XSLFTextBox.class) {
        String text = ((XSLFTextBox) shape).getText();
        Matcher matcher = PAGE_PATTERN.matcher(text);
        if (matcher.matches()) {
          pageNum = new PageNumber(Integer.parseInt(matcher.group(1)),
                                   Integer.parseInt(matcher.group(2)));
        }
      }
    }
    return pageNum;
  }
}
