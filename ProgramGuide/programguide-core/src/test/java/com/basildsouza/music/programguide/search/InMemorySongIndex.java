package com.basildsouza.music.programguide.search;

import com.basildsouza.music.programguide.manage.Clearable;

import java.util.*;
import java.util.stream.Collectors;

public class InMemorySongIndex implements SongIndex, Clearable {
  private final Map<String, Map<String, SearchTokenOccuranceCount>> occurrences = new HashMap<>();

  @Override
  public void addEntry(String searchToken, String songKey, int titleCount, int totalCount) {
    occurrences.putIfAbsent(searchToken, new HashMap<>());
    occurrences.get(searchToken).put(songKey, new SearchTokenOccuranceCount(titleCount, totalCount));
  }

  @Override
  public Map<String, SearchTokenOccuranceCount> find(String searchToken) {
    return occurrences.getOrDefault(searchToken, Collections.emptyMap());
  }

  @Override
  public Map<String, SearchTokenOccuranceCount> refineFind(String searchToken, Set<String> songKeys) {
    return find(searchToken).entrySet()
      .stream()
      .filter(e -> songKeys.contains(e.getKey()))
      .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }

  @Override
  public Map<String, SearchTokenOccuranceCount> findBySongKey(String songKey) {
    return occurrences.entrySet()
                      .stream()
                      .filter(e -> e.getValue().containsKey(songKey))
                      .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().get(songKey)));
  }

  @Override
  public void removeEntries(String songKey) {
    occurrences.values().forEach(m -> m.remove(songKey));
  }

  @Override
  public void clear() {
    occurrences.clear();
  }
}
