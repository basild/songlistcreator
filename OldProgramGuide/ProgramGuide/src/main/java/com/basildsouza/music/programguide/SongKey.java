package com.basildsouza.music.programguide;

public class SongKey {
  private final long songID;
  
  public SongKey(long songID) {
    this.songID = songID;
  }
  
  public long getSongID() {
    return songID;
  }

  @Override
  public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + (int) (songID ^ (songID >>> 32));
	return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    SongKey other = (SongKey) obj;
    if (songID != other.songID)
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "SongKey [songID=" + songID + "]";
  }
}
