package com.basildsouza.music.songpresentor.slideshow;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.basildsouza.music.programguide.Playlist;
import com.basildsouza.music.programguide.SongKey;
import com.basildsouza.music.programguide.index.LyricsTokenizer;
import com.basildsouza.music.programguide.index.SearchEntry;
import com.basildsouza.music.programguide.index.SongOccurance;
import com.basildsouza.music.programguide.index.SongSearcher;

public class SlideshowSearcherPrototype {
  private static final String BASE = "F:\\Projects\\Java\\Slideshow Splitter\\SongPresenter";
  private static final String DATA = BASE + "\\data";
  private static final String CONF = BASE + "\\conf";
  private static final String OUTPUT = BASE + "\\output";
  
  
  private static LyricsTokenizer lyricsTokenizer;
  private static SongSearcher songSearcher;
  private static SlideShowStore slideShowStore;
  private static SlideshowExporter playlistExporter;
  private static IndividualSlideShowImporter slideShowImporter;
  
  static {
    System.setProperty("template.dir", CONF);
    System.setProperty("playlist.powerpoint.template", "template.pptx");
    ApplicationContext context = new ClassPathXmlApplicationContext("/core.context.xml");
    playlistExporter = context.getBean("playlistExporter", SlideshowExporter.class);
    slideShowImporter = context.getBean("slideShowImporter", IndividualSlideShowImporter.class);
    songSearcher = context.getBean("songSearcher", SongSearcher.class);
    slideShowStore = context.getBean("slideShowStore", SlideShowStore.class);
    lyricsTokenizer = context.getBean("lyricsTokenizer", LyricsTokenizer.class);
  }
  
  public static void main(String[] args) throws FileNotFoundException, IOException{
    //playlistExporter.setTemplate(new File(CONF, "template.pptx"));
    slideShowImporter.importSlides(new File(DATA));
    
    BufferedReader keyIn = new BufferedReader(new InputStreamReader(System.in));
    String tempString = keyIn.readLine();
    while(tempString != null && !tempString.isEmpty()){
      List<SongOccurance> foundSongs = songSearcher.findSong(createSearchEntry(tempString));
      printResults(tempString, foundSongs);
      
      tempString = keyIn.readLine();
    }
  }

  private static void printResults(String tempString, List<SongOccurance> foundSongs) throws FileNotFoundException, IOException {
    System.out.println("Results for: " + tempString);
    if(foundSongs.isEmpty()){
      System.out.println("No results");
      return;
    }
    for(SongOccurance songOccurance : foundSongs){
      System.out.println("    " + songOccurance.getWeight() + " - " + 
                         slideShowStore.getSongEntry(songOccurance.getSongKey())
                                       .getSong().getTitle());
    }
    
    List<SongKey> songKeys = new ArrayList<SongKey>();
    for(SongOccurance songOccurance : foundSongs){
      songKeys.add(songOccurance.getSongKey());
    }
    
    XMLSlideShow xmlSlideShow = playlistExporter.exportPlaylist(new Playlist(songKeys));
    savePresentation(xmlSlideShow, "test-output-" + tempString + "-"+ System.currentTimeMillis());
  }
  
  private static SearchEntry createSearchEntry(String fullSearch){
    return new SearchEntry(fullSearch, lyricsTokenizer.tokenize(fullSearch));
  }
  
  private static void savePresentation(XMLSlideShow splitPpt, String title) throws FileNotFoundException, IOException{
    System.out.println("Saving: " + title + " " + splitPpt.getSlides().size());
    File outDir = new File(OUTPUT);
    File outFile = new File(outDir, title + ".pptx");
    splitPpt.write(new FileOutputStream(outFile));
  }

}
