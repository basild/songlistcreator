package com.basildsouza.music.programguide.dal;

import java.util.List;

import com.basildsouza.music.programguide.dal.entity.Song;

public interface SongDAO {
	public Song getSong(Long songID);
	
	public Song addSong(Song song);
	
	public void deleteSong(Long songID);
	
	public List<Song> listAllSongs(int pageNumber, int pageSize);
	
	public List<Song> listSongsByID(List<Long> songIDs);
	
	public List<Song> searchSongByTitle(String titleRegExp);
	
	public List<Song> searchSongByTitleAndContent(String searchRegExp);
	
	//Dont use this yet
	@Deprecated()
	public List<Song> searchSongByTag(String tagName);
}
