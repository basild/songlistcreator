package com.basildsouza.music.programguide.search;

import java.util.List;

public interface SongSearchEngine {
  List<SongSearchResult> findSong(String searchEntry);
}
