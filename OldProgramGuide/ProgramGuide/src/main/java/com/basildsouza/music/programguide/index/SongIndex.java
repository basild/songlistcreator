package com.basildsouza.music.programguide.index;

import java.util.Map;
import java.util.Set;

import com.basildsouza.music.programguide.SongKey;

public interface SongIndex {
  void addIndexEntry(String searchTerm, SongKey songKey, 
                            int titleCount, int bodyCount);
  
  Map<SongKey, SongIndexEntry> findIndexEntry(String searchToken);
  
  void removeIndexEntry(SongKey songKey, Set<String> uniqueWords);
}
