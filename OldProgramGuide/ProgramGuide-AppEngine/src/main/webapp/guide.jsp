<!DOCTYPE html>
<html>
<head>
<title>My Page</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="css/program-guide.css" />
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script
	src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
    	console.log("test");
  	});
</script>
</head>
<body>

	<div data-role="page">
		<div data-role="header">
			<a href="index.html" data-icon="delete">Logout</a>
			<h1>Program Guide Creator</h1>
			<a href="index.html" data-icon="gear">Options</a>
			<div data-role="navbar">
				<ul>
					<li><a href="song-index.html">Search</a></li>
					<li><a href="song-add.html">Add Song</a></li>
				</ul>
			</div>
			<!-- /navbar -->
		</div>
		<!-- /header -->
		<div data-role="content">
		<div data-role="collapsible">
   			<h3>First Song Title - Key: C - Timing: 3/4</h3>
   				<p>
   				This is the lyrics<br/>
   				Another line of the song<br/>
   				etc <br/>
  				</p>
		</div>
		<div data-role="collapsible">
		
   			<h3>First Song Title</h3>
   				<p>
   				This is the lyrics<br/>
   				Another line of the song<br/>
   				etc <br/>
  				</p>
		</div>
		<div data-role="collapsible">
   			<h3>First Song Title</h3>
   				<p>
   				This is the lyrics<br/>
   				Another line of the song<br/>
   				etc <br/>
  				</p>
		</div>
		</div>
		<!-- /content -->

	</div>
	<!-- /page -->

</body>
</html>