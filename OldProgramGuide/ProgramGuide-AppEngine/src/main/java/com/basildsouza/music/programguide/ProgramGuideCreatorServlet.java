package com.basildsouza.music.programguide;

import java.io.IOException;

import javax.servlet.http.*;

import com.basildsouza.music.programguide.dal.SongDAO;
import com.basildsouza.music.programguide.dal.entity.Song;
import com.basildsouza.music.programguide.dal.jdoimpl.GAESongDAOImpl;

@SuppressWarnings("serial")
public class ProgramGuideCreatorServlet extends HttpServlet {
	SongDAO testDAO = new GAESongDAOImpl();
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		resp.getWriter().println("Hello, world");
		
		if(req.getParameter("create") != null){
			Song song = new Song();
			song.setTitle("Test Song Title");
			song.setTiming("3/4");
			song.setRhythm("3/4");
			song.setKey("C");
			song.setLyrics("sadfasdfasdfasdf dsfasdf asdf");
			song.setType("PlainText");
			song.setReferenceURL("asdfasdf");
			Song addedSong = testDAO.addSong(song);
			resp.getWriter().print("written song: " + addedSong);
		}
		
		if(req.getParameter("list") != null){
			resp.getWriter().println(testDAO.getSong(Long.parseLong(req.getParameter("list"))));
		}
		
		if(req.getParameter("listAll") != null){
			resp.getWriter().println(testDAO.listAllSongs(0, 100));
		}
		
	}
}
