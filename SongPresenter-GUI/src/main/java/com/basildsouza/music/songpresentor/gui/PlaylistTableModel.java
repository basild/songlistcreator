package com.basildsouza.music.songpresentor.gui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.basildsouza.music.programguide.Playlist;
import com.basildsouza.music.programguide.SongKey;

public class PlaylistTableModel extends AbstractTableModel {
  private static final long serialVersionUID = 1L;
  
  private final List<PlaylistEntryRow> playlistEntry = new ArrayList<PlaylistEntryRow>();
  private final String[] columnNames = {"Title", "Pages"};
  
  public void clearAllPlaylistEntries(){
    playlistEntry.clear();
    fireTableDataChanged();
  }
  
  public void addPlaylistEntry(PlaylistEntryRow entryRow){
    playlistEntry.add(entryRow);
    fireTableDataChanged();
  }
  
  public void removePlaylistEntry(int index){
    playlistEntry.remove(index);
    fireTableDataChanged();
  }
  
  public void movePlaylistEntryUp(int index){
    if(index == 0){
      return;
    }
    PlaylistEntryRow playlistEntryRow = playlistEntry.remove(index);
    playlistEntry.add(index-1, playlistEntryRow);
    
    fireTableDataChanged();
  }
  
  public void movePlaylistEntryDown(int index){
    if(index >= playlistEntry.size()-1){
      return;
    }
    
    PlaylistEntryRow playlistEntryRow = playlistEntry.remove(index);
    playlistEntry.add(index+1, playlistEntryRow);
    
    fireTableDataChanged();
  }
  
  public Playlist getPlaylist(){
    List<SongKey> songKeys = new ArrayList<SongKey>(playlistEntry.size());
    for(PlaylistEntryRow entryRow : playlistEntry){
      songKeys.add(entryRow.getSongKey());
    }
    return new Playlist(songKeys);
  }

  @Override
  public String getColumnName(int column) {
    return columnNames[column];
  }
  
  @Override
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    return false;
  }
  
  public int getColumnCount() {
    return 2;
  }

  public int getRowCount() {
    return playlistEntry.size();
  }

  public Object getValueAt(int row, int col) {
    PlaylistEntryRow playlistRow = playlistEntry.get(row);
    if(col == 0){
      return playlistRow.getTitle();
    } else if (col == 1){
      return playlistRow.getNumPages();
    }
    return "???";
  }
}