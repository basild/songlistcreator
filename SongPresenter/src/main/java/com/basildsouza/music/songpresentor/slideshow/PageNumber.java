package com.basildsouza.music.songpresentor.slideshow;

class PageNumber {
  private final int pageNumber;
  private final int maxPages;

  public PageNumber(int pageNumber, int maxPages) {
    this.pageNumber = pageNumber;
    this.maxPages = maxPages;
  }

  public int getMaxPages() {
    return maxPages;
  }

  public int getPageNumber() {
    return pageNumber;
  }

  public boolean isLastPage() {
    return pageNumber == maxPages;
  }

  @Override
  public String toString() {
    return "PageNumber [pageNumber=" + pageNumber + ", maxPages=" + maxPages
        + "]";
  }

}