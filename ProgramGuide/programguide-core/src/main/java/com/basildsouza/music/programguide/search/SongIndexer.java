package com.basildsouza.music.programguide.search;

public interface SongIndexer {
  void addSong(String songKey, String title, String body);

  void editSong(String songKey, String title, String body);

  void removeSong(String songKey);
}
