package com.basildsouza.music.programguide.index;

public class SongIndexEntry {
  private final int titleCount;
  private final int bodyCount;
  
  public SongIndexEntry(int titleCount, int bodyCount) {
    this.titleCount = titleCount;
    this.bodyCount = bodyCount;
  }

  public int getBodyCount() {
    return bodyCount;
  }
  
  public int getTitleCount() {
    return titleCount;
  }

  @Override
  public String toString() {
    return "SongIndexEntry [titleCount=" + titleCount + ", bodyCount="
        + bodyCount + "]";
  }
}
