package com.basildsouza.music.programguide.services.rest;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.basildsouza.music.programguide.dal.SongDAO;
import com.basildsouza.music.programguide.dal.entity.Song;
import com.basildsouza.music.programguide.dal.jdoimpl.GAESongDAOImpl;

@Path("/song/")
public class SongService {
	private final static int DEFAULT_PAGE_SIZE = 400;
	
	//TODO: Inject it
	private final SongDAO songDAO = new GAESongDAOImpl();
	private final Logger logger = Logger.getLogger(SongService.class.getName());
	
	@GET
	@Path("{id}")
	@Produces("application/json")
	public Song getSong(@PathParam("id") Long songID){
		return songDAO.getSong(songID);
	}
	
	@POST
	public Response addSong(@FormParam("songID") String songID,
							@FormParam("title") String title, 
							@FormParam("lyrics") String lyrics, 
							@FormParam("key") String key, 
							@FormParam("timing") String timing,
							@FormParam("rhythm") String rhythm,
							@FormParam("referenceURL") String referenceURL, 
							@FormParam("type") String type){
		
		final Song song = new Song();
		if(songID != null && !songID.isEmpty()){
			logger.info("Song ID: " + songID);
			song.setSongID(Long.parseLong(songID));
		}
		song.setTitle(title);
		song.setLyrics(lyrics);
		song.setKey(key);
		song.setTiming(timing);
		song.setRhythm(rhythm);
		song.setReferenceURL(referenceURL);
		song.setType(type);
		
		logger.info("Pretending to Adding Song: " + song);
		final Song addedSong;
		try {
			addedSong = songDAO.addSong(song);
		} catch (Exception ex){
			return Response.serverError().build();
		}
		logger.info("Added Song With ID " + addedSong.getSongID());
		return Response.ok().build();
	}
	
	@DELETE
	@Path("{songID}")
	public Response deleteSong(@PathParam("songID") Long songID){
		logger.info("Deleting song by ID: " + songID);
		songDAO.deleteSong(songID);
		
		return Response.ok().build();
	}
	
	@GET
    @Path("page/{page}/size/{size}/")
	@Produces("application/json")
	public List<Song> listSongsByPage(@PathParam("page") int page, 
						  		@PathParam("size") int size){
		logger.info("Listing songs for page: " + page + " Page size: " + size);
		return songDAO.listAllSongs(page, size);
	}
	
	@GET
	@Path("page/{page}/")
	@Produces("application/json")
	public List<Song> listSongsByPage(@PathParam("page") int page){
		return listSongsByPage(page, DEFAULT_PAGE_SIZE);
	}
	
	@GET
	@Path("list")
	@Produces("application/json")
	public List<Song> listMultipleSongs(@QueryParam("s") List<Long> songIDs){
		logger.info("Listing songs by id for list: " + songIDs);
		return songDAO.listSongsByID(songIDs);
	}

//	@GET
//	@Path("list")
//	@Produces("application/json")
//	public List<Song> listSongsByPrefix(@QueryParam("prefix") String prefix){
//		logger.info("Listing songs by id for list: " + songIDs);
//		return songDAO.listSongsByID(songIDs);
//	}
}
