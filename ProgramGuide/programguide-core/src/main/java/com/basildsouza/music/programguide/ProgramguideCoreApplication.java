package com.basildsouza.music.programguide;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgramguideCoreApplication {

  public static void main(String[] args) {
    SpringApplication.run(ProgramguideCoreApplication.class, args);
  }
}
