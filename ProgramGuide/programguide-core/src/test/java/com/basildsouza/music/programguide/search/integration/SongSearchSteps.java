package com.basildsouza.music.programguide.search.integration;

import com.basildsouza.music.programguide.search.SongSearchEngine;
import com.basildsouza.music.programguide.search.SongSearchResult;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

import static org.junit.Assert.assertEquals;

//@ContextConfiguration(classes = SearchEngineIntegrationConfig.class)
//@ActiveProfiles("Persisted")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SongSearchSteps {
  private final SongSearchEngine searchEngine;

  private List<SongSearchResult> searchResult;

  @When("^searching for Term: '(.*?)'$")
  public void searchingForTermSearchTerm(String searchTerm) {
    searchResult = searchEngine.findSong(searchTerm);
  }

  @Then("^'(\\d+?)' results? should be found$")
  public void numResultsShouldBeFound(int numResults) {
    assertEquals(numResults, searchResult.size());
  }

  @And("^key '(.+?)' at rank '(\\d+)'$")
  public void keyAtRank(String songKey, int rank) throws Throwable {
    assertEquals(songKey, searchResult.get(rank-1).getSongKey());
  }
}
