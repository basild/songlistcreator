package com.basildsouza.music.programguide.search;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Component
public class HardcodedCommonWordExcluder implements CommonWordExcluder {
  private final Set<String> excludedWords = Collections.unmodifiableSet(new HashSet<>(Arrays.asList("A", "THE", "AN")));

  @Override
  public boolean isExcluded(String word) {
    return excludedWords.contains(word);
  }

  @Override
  public boolean isIncluded(String word) {
    return !isExcluded(word);
  }
}
