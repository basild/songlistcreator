package com.basildsouza.music.songpresentor.index;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.basildsouza.music.programguide.SongKey;
import com.basildsouza.music.programguide.index.CommonWordExcluder;
import com.basildsouza.music.programguide.index.SearchEntry;
import com.basildsouza.music.programguide.index.SongIndex;
import com.basildsouza.music.programguide.index.SongIndexEntry;
import com.basildsouza.music.programguide.index.SongOccurance;
import com.basildsouza.music.programguide.index.SongSearcher;
import com.basildsouza.music.programguide.index.SongOccurance.Builder;

public class DefaultSongSearcher implements SongSearcher{
  
  private final SongIndex songIndex;
  private final CommonWordExcluder excluder;

  public DefaultSongSearcher(SongIndex songIndex, CommonWordExcluder excluder) {
    this.songIndex = songIndex;
    this.excluder = excluder;
  }

  public List<SongOccurance> findSong(SearchEntry searchEntry){
    List<String> searchableTokens = excluder.removeCommonWords(searchEntry.getTokenizedTerms());
    Map<SongKey, SongOccurance.Builder> foundResults = new HashMap<SongKey, SongOccurance.Builder>();
    for(String searchToken : searchableTokens){
      Map<SongKey, SongIndexEntry> currentOccurances = songIndex.findIndexEntry(searchToken); 
      if(foundResults.isEmpty()){
        copyOverOccurances(foundResults, currentOccurances);
        continue;
      }
      filterNextSearchTerm(foundResults, currentOccurances);
      
      if(foundResults.isEmpty()){
        break;
      }
    }
    List<SongOccurance> songOccurances = buildResultList(foundResults);
    
    return songOccurances;
  }

  private List<SongOccurance> buildResultList(
      Map<SongKey, SongOccurance.Builder> foundResults) {
    List<SongOccurance> songOccurances = new ArrayList<SongOccurance>(foundResults.size());
    for(Entry<SongKey, SongOccurance.Builder> foundEntry : foundResults.entrySet()){
      songOccurances.add(foundEntry.getValue().build());
    }
    
    Collections.sort(songOccurances, new Comparator<SongOccurance>() {
      public int compare(SongOccurance o1, SongOccurance o2) {
        return o1.getWeight().compareTo(o2.getWeight()) * -1;
      }
    });
    return songOccurances;
  }

  private void copyOverOccurances(Map<SongKey, SongOccurance.Builder> foundResults,
                                  Map<SongKey, SongIndexEntry> currentOccurances) {
    for(Entry<SongKey, SongIndexEntry> occurance : currentOccurances.entrySet()){
      SongIndexEntry songIndexEntry = occurance.getValue();
      foundResults.put(occurance.getKey(), 
                       new SongOccurance.Builder(occurance.getKey()).setBodyOccurance(songIndexEntry.getBodyCount())
                                                                    .setTitleOccurance(songIndexEntry.getTitleCount()));
    }
  }

  private void filterNextSearchTerm(Map<SongKey, Builder> foundResults,
                                    Map<SongKey, SongIndexEntry> currentOccurances) {
    Iterator<Entry<SongKey, SongOccurance.Builder>> foundIterator = foundResults.entrySet().iterator();
    while(foundIterator.hasNext()){
      Entry<SongKey, Builder> foundEntry = foundIterator.next();
      SongKey foundSongKey = foundEntry.getKey();
      if(!currentOccurances.containsKey(foundSongKey)){
        foundIterator.remove();
      } else {
        SongIndexEntry songIndexEntry = currentOccurances.get(foundSongKey);
        foundEntry.getValue().setBodyOccurance(songIndexEntry.getBodyCount())
                             .setTitleOccurance(songIndexEntry.getTitleCount());
      }
    }
  }

}
