package com.basildsouza.music.songpresentor.slideshow;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xslf.usermodel.XMLSlideShow;

public class PoiSlideshowReader implements SlideShowReader {
  public XMLSlideShow readSlide(File pptFile) throws FileNotFoundException, IOException{
    System.out.println("Loading: " + pptFile.getName());
    return new XMLSlideShow(new FileInputStream(pptFile));
  }
}
