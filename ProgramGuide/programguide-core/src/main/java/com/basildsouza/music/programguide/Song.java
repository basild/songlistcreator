package com.basildsouza.music.programguide;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Song {
  private final String songKey;
  private final String title;
  private final String key;
  private final String timing;
  private final String body;

  //TODO: Incomplete - Currently just a placeholder
}
