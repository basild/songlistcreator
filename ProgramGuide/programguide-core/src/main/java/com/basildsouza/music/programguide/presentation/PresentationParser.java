package com.basildsouza.music.programguide.presentation;

import java.util.List;
import java.util.Map;

public interface PresentationParser {
  ParsedPresentation parse(String presentationKey);

  List<Map.Entry<ParsedPresentation, byte[]>> split(byte[] combinedPresentation);
}
