package com.basildsouza.music.programguide.search.integration;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@RunWith(Cucumber.class)
@CucumberOptions(
  features = {"classpath:features/search.feature",
              "classpath:features/search-ranking.feature",},
  format = {"pretty"}
)
@ActiveProfiles("Persisted")
@ContextConfiguration(classes = SearchEngineIntegrationConfig.class)
public class SearchEngineIntegrationTest {

}

@TestConfiguration
@EnableAutoConfiguration
@ComponentScan("com.basildsouza.music.programguide.search.integration")
@ComponentScan("com.basildsouza.music.programguide.search")
class SearchEngineIntegrationConfig {
}
