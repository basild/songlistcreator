package com.basildsouza.music.songpresentor.index;

import java.util.Arrays;
import java.util.List;

import com.basildsouza.music.programguide.index.CommonWordExcluder;

public class HardcodedCommonWordExcluder implements CommonWordExcluder{
  List<String> excludedWords = Arrays.asList("A", "THE", "AN");
  public List<String> removeCommonWords(List<String> individualWords) {
    while(individualWords.removeAll(excludedWords)){}
    return individualWords;
  }
}
