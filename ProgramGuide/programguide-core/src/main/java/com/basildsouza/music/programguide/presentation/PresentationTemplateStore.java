package com.basildsouza.music.programguide.presentation;

import java.util.List;

public interface PresentationTemplateStore extends PresentationStore {
  List<String> getAllTemplateNames();
}
