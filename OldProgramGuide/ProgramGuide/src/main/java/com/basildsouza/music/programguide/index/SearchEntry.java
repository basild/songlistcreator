package com.basildsouza.music.programguide.index;

import java.util.List;

public class SearchEntry {
  private final String fullEntry;
  private final List<String> tokenizedTerms;
  
  public SearchEntry(String fullEntry, List<String> tokenizedTerms) {
    this.fullEntry = fullEntry;
    this.tokenizedTerms = tokenizedTerms;
  }
  
  public String getFullEntry() {
    return fullEntry;
  }
  
  public List<String> getTokenizedTerms() {
    return tokenizedTerms;
  }

  @Override
  public String toString() {
    return "SearchEntry [fullEntry=" + fullEntry + ", tokenizedTerms="
        + tokenizedTerms + "]";
  }
}
