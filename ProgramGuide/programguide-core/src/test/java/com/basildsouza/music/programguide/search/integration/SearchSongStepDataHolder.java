package com.basildsouza.music.programguide.search.integration;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
@Getter
public class SearchSongStepDataHolder {
  private final SingleSongSearchData.SingleSongSearchDataBuilder singleSearchSongBuilder = SingleSongSearchData.builder().songKey("1");
}

@Builder
@Data
class SingleSongSearchData {
  private final String songKey;
  private final String title;
  private final String body;
}
