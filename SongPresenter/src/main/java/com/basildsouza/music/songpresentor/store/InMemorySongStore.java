package com.basildsouza.music.songpresentor.store;

import java.util.HashMap;
import java.util.Map;

import com.basildsouza.music.programguide.SongEntry;
import com.basildsouza.music.programguide.SongKey;
import com.basildsouza.music.programguide.store.SongStore;

public class InMemorySongStore implements SongStore{
  private final Map<SongKey, SongEntry> songEntries = new HashMap<SongKey, SongEntry>();
  
  public SongEntry getSongEntry(SongKey songKey) {
    return songEntries.get(songKey);
  }

  public void addSongEntry(SongEntry songEntry) {
    songEntries.put(songEntry.getSongKey(), songEntry);
  }
}
