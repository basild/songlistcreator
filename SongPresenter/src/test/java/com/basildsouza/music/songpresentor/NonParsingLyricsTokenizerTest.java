package com.basildsouza.music.songpresentor;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import com.basildsouza.music.songpresentor.index.NonParsingLyricsTokenizer;

public class NonParsingLyricsTokenizerTest {
  private final NonParsingLyricsTokenizer systemUnderTest = new NonParsingLyricsTokenizer();
  
  @Test
  public void testSpaceSeparateSingleLine() {
    assertTermSplitsTo("TERM1 TERM2", "TERM1", "TERM2");
  }
  
  @Test
  public void testNewLineSeparate() {
    assertTermSplitsTo("TERM1\nTERM2", "TERM1", "TERM2");
  }
  
  @Test
  public void testNewLineAndSpaceSeparate() {
    assertTermSplitsTo("TERM1 \n TERM2", "TERM1", "TERM2");
  }

  @Test
  public void testPunctuationRemoval() {
    assertTermSplitsTo("TERM1. TERM2, TERM3! TERM4", "TERM1", "TERM2", "TERM3", "TERM4");
  }
  
  @Test
  public void testCaseFxing() {
    assertTermSplitsTo("Term1", "TERM1");
  }
  
  
  private void assertTermSplitsTo(String term, String... splitsTo){
    assertEquals(Arrays.asList(splitsTo), systemUnderTest.tokenize(term));
  }

  
}
