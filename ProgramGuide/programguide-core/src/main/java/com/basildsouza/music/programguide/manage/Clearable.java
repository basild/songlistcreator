package com.basildsouza.music.programguide.manage;

public interface Clearable {
  void clear();
}
