package com.basildsouza.music.programguide.search;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class DefaultSongIndexer implements SongIndexer {
  private final SongIndex songIndex;
  private final LyricsTokenizer lyricsTokenizer;
  private final CommonWordExcluder excluder;

  @Override
  public void addSong(String songKey, String title, String body) {
    final Map<String, Long> titleOccurrences = countWordOccurances(title);
    final Map<String, Long> totalOccurrences = countWordOccurances(body);

    titleOccurrences.forEach((k, v) -> totalOccurrences.put(k, totalOccurrences.getOrDefault(k, 0L) + v));

    for (Map.Entry<String, Long> wordEntry : totalOccurrences.entrySet()) {
      String searchToken = wordEntry.getKey();
      int titleCount = titleOccurrences.getOrDefault(searchToken, 0L).intValue();
      int totalCount = wordEntry.getValue().intValue();
      songIndex.addEntry(searchToken, songKey, titleCount, totalCount);
    }
  }

  @Override
  public void editSong(String songKey, String title, String body) {
    // Currently a very simple implementation, can be made more efficient later
    removeSong(songKey);
    addSong(songKey, title, body);
  }

  @Override
  public void removeSong(String songKey) {
    songIndex.removeEntries(songKey);
  }

  private Map<String, Long> countWordOccurances(String text) {
    return lyricsTokenizer.tokenize(text)
      .filter(excluder::isIncluded)
      .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
  }
}
