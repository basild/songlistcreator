package com.basildsouza.music.programguide.search;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Stream;

@Component
public class NonParsingLyricsTokenizer implements LyricsTokenizer {
  @Override
  public Stream<String> tokenize(String songText) {
    return Arrays.stream(songText.split("\\W"))
      .filter(w -> !w.isEmpty())
      .map(String::toUpperCase);
  }
}
