package com.basildsouza.music.songpresentor.slideshow;

import java.util.HashMap;
import java.util.Map;

import org.apache.poi.xslf.usermodel.XMLSlideShow;

import com.basildsouza.music.programguide.SongEntry;
import com.basildsouza.music.programguide.SongKey;
import com.basildsouza.music.programguide.store.SongStore;

public class InMemorySlideshowStore implements SlideShowStore {
  private final SongStore songStore;
  private final Map<SongKey, XMLSlideShow> slideShows = new HashMap<SongKey, XMLSlideShow>();
  
  public InMemorySlideshowStore(SongStore songStore) {
    this.songStore = songStore;
  }
  
  public void addSong(SongEntry songEntry, XMLSlideShow slideShow){
    songStore.addSongEntry(songEntry);
    slideShows.put(songEntry.getSongKey(), slideShow);
  }
  
  public XMLSlideShow getSlideShow(SongKey songKey){
    return slideShows.get(songKey);
  }
  
  public SongEntry getSongEntry(SongKey songKey){
    return songStore.getSongEntry(songKey);
  }
}
