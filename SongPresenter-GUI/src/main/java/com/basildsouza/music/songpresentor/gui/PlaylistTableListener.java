package com.basildsouza.music.songpresentor.gui;

import javax.swing.JButton;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

class PlaylistTableListener implements TableModelListener {
  private final PlaylistTableModel playlistTableModel;
  private final JButton[] buttonsToDisable;

  public PlaylistTableListener(PlaylistTableModel playlistTableModel,
                               JButton... buttonsToDisable) {
    this.playlistTableModel = playlistTableModel;
    this.buttonsToDisable = buttonsToDisable;
  }
  
  public void tableChanged(TableModelEvent e) {
    boolean tableEmpty = playlistTableModel.getRowCount() == 0;
    for(JButton buttonToDisable : buttonsToDisable){
      buttonToDisable.setEnabled(!tableEmpty);
    }
  }
  
}