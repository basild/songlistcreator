package com.basildsouza.music.songpresentor.slideshow;

import org.apache.poi.xslf.usermodel.XMLSlideShow;

import com.basildsouza.music.programguide.SongEntry;
import com.basildsouza.music.programguide.SongKey;

public interface SlideShowStore {

  public abstract void addSong(SongEntry songEntry, XMLSlideShow slideShow);

  public abstract XMLSlideShow getSlideShow(SongKey songKey);

  public abstract SongEntry getSongEntry(SongKey songKey);

}