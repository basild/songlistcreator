package com.basildsouza.music.songpresentor.index;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.basildsouza.music.programguide.Song;
import com.basildsouza.music.programguide.SongEntry;
import com.basildsouza.music.programguide.SongKey;
import com.basildsouza.music.programguide.index.CommonWordExcluder;
import com.basildsouza.music.programguide.index.LyricsTokenizer;
import com.basildsouza.music.programguide.index.SearchEntry;
import com.basildsouza.music.programguide.index.SongIndex;
import com.basildsouza.music.programguide.index.SongKeyGenerator;
import com.basildsouza.music.programguide.index.SongOccurance;


public class DefaultSongIndexerTest {
  private CommonWordExcluder excluder = new HardcodedCommonWordExcluder();
  private LyricsTokenizer lyricsTokenizer = new NonParsingLyricsTokenizer();
  private SongKeyGenerator songKeyGenerator = new InMemorySongKeyGenerator();
  private SongIndex songIndex = new InMemorySongIndex();

  private DefaultSongIndexer systemUnderTest = new DefaultSongIndexer(songKeyGenerator, lyricsTokenizer, excluder, songIndex);
  private DefaultSongSearcher songSearcher = new DefaultSongSearcher(songIndex, excluder);
  
  @Test
  public void noMatchesTest() {
    systemUnderTest.addSong(createDummySong("TERM1 TERM2"));
    List<SongOccurance> findSong = songSearcher.findSong(createDummySearchEntry("TERM3"));
    assertEquals(0, findSong.size());
  }
  
  @Test
  public void singleSongTest() {
    SongEntry songEntry = systemUnderTest.addSong(createDummySong("TERM1 TERM2"));
    List<SongOccurance> findSong = songSearcher.findSong(createDummySearchEntry("TERM1", "TERM2"));
    assertEquals(1, findSong.size());
    assertContainsSongWithKey(findSong, songEntry.getSongKey());
  }
  
  @Test
  public void multipleSongsReturnedTest() {
    SongEntry songEntry = systemUnderTest.addSong(createDummySong("TERM1 TERM2"));
    systemUnderTest.addSong(createDummySong("TERM1 TERM3"));
    List<SongOccurance> findSong = songSearcher.findSong(createDummySearchEntry("TERM1", "TERM2"));
    assertEquals(1, findSong.size());
    assertEquals(songEntry.getSongKey(), findSong.get(0).getSongKey());
    assertContainsSongWithKey(findSong, songEntry.getSongKey());
  }
  
  @Test
  public void multipleSongsReturned(){
    SongEntry songEntry1 = systemUnderTest.addSong(createDummySong("TERM1 TERM2"));
    SongEntry songEntry2 = systemUnderTest.addSong(createDummySong("TERM2 TERM3"));
    systemUnderTest.addSong(createDummySong("TERM1 TERM4"));
    List<SongOccurance> findSong = songSearcher.findSong(createDummySearchEntry("TERM2"));
    assertEquals(2, findSong.size());
    assertContainsSongWithKey(findSong, songEntry1.getSongKey());
    assertContainsSongWithKey(findSong, songEntry2.getSongKey());
  }
  
  @Test
  public void multipleResultsDiffWeightsTest() {
    systemUnderTest.addSong(createDummySong("TERM1 TERM2"));
    SongEntry songEntry = systemUnderTest.addSong(createDummySong("TERM1 TERM2 TERM1"));
    List<SongOccurance> findSong = songSearcher.findSong(createDummySearchEntry("TERM1", "TERM2"));
    assertEquals(2, findSong.size());
    assertEquals(songEntry.getSongKey(), findSong.get(0).getSongKey());
    assertTrue(findSong.get(0).getWeight() > findSong.get(1).getWeight());
  }

  @Test
  public void multipleResultsTitleWeighedModeTest() {
    systemUnderTest.addSong(createDummySongWithTitle("DUMMY TITLE", "TERM1 TERM2"));
    SongEntry songEntry = systemUnderTest.addSong(createDummySongWithTitle("TERM2", "TERM1"));
    List<SongOccurance> findSong = songSearcher.findSong(createDummySearchEntry("TERM2"));
    assertEquals(2, findSong.size());
    assertEquals(songEntry.getSongKey(), findSong.get(0).getSongKey());
    assertTrue(findSong.get(0).getWeight() > findSong.get(1).getWeight());
  }

  
  private void assertContainsSongWithKey(List<SongOccurance> songList,
      SongKey songKey) {
    for(SongOccurance songOccurance : songList){
      if(songOccurance.getSongKey().equals(songKey)){
        return;
      }
    }
    fail("Did not find song occurance: " + songKey);
  }

  private SearchEntry createDummySearchEntry(String... tokenizedTerms){
    return new SearchEntry("Whatever", Arrays.asList(tokenizedTerms));
  }
  
  private Song createDummySong(String body){
    return createDummySongWithTitle("Dummy Title", body);
  }

  private Song createDummySongWithTitle(String title, String body) {
    return new Song(title, "X", "Y", body);
  }
  
  
}
