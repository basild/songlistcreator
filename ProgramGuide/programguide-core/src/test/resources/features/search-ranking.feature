Feature: Ranking of search results

    Scenario: Title ranked over body
        Given song indexed with key '1' and title 'TERM1' and body 'A Body'
        And   song indexed with key '2' and title 'The title' and body 'TERM1'
        When searching for Term: 'TERM1'
        Then '2' results should be found
        And  key '1' at rank '1'

    Scenario: Multiple ranked over single
        Given song indexed with key '1' and title 'The title' and body 'TERM1'
        And   song indexed with key '2' and title 'The title' and body 'TERM1 TERM1'
        When searching for Term: 'TERM1'
        Then '2' results should be found
        And  key '2' at rank '1'
