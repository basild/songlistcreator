package com.basildsouza.music.programguide.search;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Embeddable;
import java.io.Serializable;

@AllArgsConstructor
@Embeddable
@Getter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class SongIndexEntryKey implements Serializable {
  private String searchToken;
  private String songKey;
}
