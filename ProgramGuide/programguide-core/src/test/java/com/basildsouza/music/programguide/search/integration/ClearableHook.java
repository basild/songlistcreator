package com.basildsouza.music.programguide.search.integration;

import com.basildsouza.music.programguide.manage.Clearable;
import cucumber.api.java.After;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

//TODO: Move this somewhere else
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ClearableHook {
  private final List<Clearable> allClearables;

  @After
  public void clearAllClearables(){
    System.out.println("Clearing clearables: " + allClearables.size());
    allClearables.forEach(Clearable::clear);
  }
}
