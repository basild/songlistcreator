package com.basildsouza.music.songpresentor;

import com.basildsouza.music.programguide.Playlist;

public interface PlaylistExporter<T> {
  T exportPlaylist(Playlist playlist);
}
