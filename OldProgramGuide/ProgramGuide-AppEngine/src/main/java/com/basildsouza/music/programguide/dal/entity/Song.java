package com.basildsouza.music.programguide.dal.entity;

import javax.jdo.annotations.FetchGroup;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Text;

@PersistenceCapable(detachable="true")
@FetchGroup(name = "lyrics-text", members = { @Persistent(name = 
"lyrics") }) 
public class Song {
	
	@Persistent
	@PrimaryKey
	private Long songID;
	
	@Persistent
	private String title;
	
	//@Persistent
	//private String lyrics;
	
	@Persistent(defaultFetchGroup = "true")
	private Text lyrics;
	
	@Persistent
	private String type;
	
	@Persistent
	private String key;
	
	@Persistent
	private String timing;

	@Persistent
	private String rhythm;

	@Persistent
	private String referenceURL;

//	public Song(Integer songID, String title, String lyrics, String type,
//			String key, String timing, String referenceURL) {
//		super();
//		this.songID = songID;
//		this.title = title;
//		this.lyrics = lyrics;
//		this.type = type;
//		this.key = key;
//		this.timing = timing;
//		this.referenceURL = referenceURL;
//	}

	public Long getSongID() {
		return songID;
	}

	public void setSongID(Long songID) {
		this.songID = songID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLyrics() {
		return lyrics.getValue();
	}

	public void setLyrics(String lyrics) {
		this.lyrics = new Text(lyrics);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getTiming() {
		return timing;
	}

	public void setTiming(String timing) {
		this.timing = timing;
	}

	public String getReferenceURL() {
		return referenceURL;
	}

	public void setReferenceURL(String referenceURL) {
		this.referenceURL = referenceURL;
	}
	
	public String getRhythm() {
		return rhythm;
	}
	
	public void setRhythm(String rhythm) {
		this.rhythm = rhythm;
	}

	@Override
	public String toString() {
		return "Song [songID=" + songID + ", title=" + title + ", lyrics="
				+ lyrics + ", type=" + type + ", key=" + key + ", timing="
				+ timing + ",rhythm=" + rhythm + ", referenceURL=" + referenceURL + "]";
	}
}