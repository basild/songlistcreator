package com.basildsouza.music.songpresentor.index;

import java.util.ArrayList;
import java.util.List;

import com.basildsouza.music.programguide.index.LyricsTokenizer;

public class NonParsingLyricsTokenizer implements LyricsTokenizer {
  public List<String> tokenize(String songText) {
    String[] split = songText.split("\\W");
    List<String> tokenList = new ArrayList<String>(split.length);
    for(int i=0; i<split.length; i++){
      if(split[i].isEmpty()){
        continue;
      }
      tokenList.add(split[i].toUpperCase());
    }
    return tokenList;
  }
}
