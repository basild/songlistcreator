package com.basildsouza.music.programguide.index;

import java.util.List;

public interface LyricsTokenizer {
  public List<String> tokenize(String songText);
}
