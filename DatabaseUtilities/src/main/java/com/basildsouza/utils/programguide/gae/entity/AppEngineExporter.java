package com.basildsouza.utils.programguide.gae.entity;

import java.io.FileWriter;
import java.io.IOException;

import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class AppEngineExporter {
	public static void main(String[] args) throws IOException{
		DefaultClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(clientConfig);
		for(int i=0; i<140; i++){
			ClientResponse clientResponse = client.resource("http://setlistcreator.basildsouza.com/rest/song/" + i).get(ClientResponse.class);
			if(clientResponse.getStatus() != 200){
				System.out.println("Skipping: " + i);
				continue;
			}
			FileWriter writer = new FileWriter("F:\\Projects\\Java\\SongListCreator\\DatabaseUtilities\\data\\Song-" + i + ".json");
			writer.write(clientResponse.getEntity(String.class));
			writer.flush();
			writer.close();
		}
		
		
	}
}
