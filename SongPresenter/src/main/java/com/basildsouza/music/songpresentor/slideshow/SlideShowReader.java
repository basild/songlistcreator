package com.basildsouza.music.songpresentor.slideshow;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xslf.usermodel.XMLSlideShow;

public interface SlideShowReader {

  XMLSlideShow readSlide(File pptFile)
      throws FileNotFoundException, IOException;

}