package com.basildsouza.music.songpresentor.gui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractListModel;

import com.basildsouza.music.programguide.SongKey;

public class SongResultListModel extends AbstractListModel<SongResultEntry>{
  private static final long serialVersionUID = 1L;
  private List<SongResultEntry> songResults = new ArrayList<SongResultEntry>();
  
  public void addAllEntries(List<SongResultEntry> entries){
    songResults = entries;
    fireContentsChanged(this, 0, entries.size());
  }
  
  public SongResultEntry getElementAt(int index) {
    return songResults.get(index);
  }

  public int getSize() {
    return songResults.size();
  }
  
  public SongKey getSongAt(int index){
    return songResults.get(index).getSongKey();
  }
}