package com.basildsouza.music.programguide.search;

import com.basildsouza.music.programguide.manage.Clearable;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Profile("Persisted")
@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class PersistedSongIndex implements SongIndex, Clearable {
  private final SongIndexRepository songIndexRepository;

  @Override
  public void addEntry(String searchToken, String songKey, int titleCount, int totalCount) {
    songIndexRepository.save(new SongIndexEntry(searchToken, songKey, new SearchTokenOccuranceCount(titleCount, totalCount)));
  }

  @Override
  public Map<String, SearchTokenOccuranceCount> find(String searchToken) {
    return songIndexRepository.findBySearchToken(searchToken)
      .stream()
      .collect(Collectors.toMap(SongIndexEntry::getSongKey, SongIndexEntry::getOccurrenceCount));
  }

  @Override
  public Map<String, SearchTokenOccuranceCount> findBySongKey(String songKey) {
    return songIndexRepository.findBySongKey(songKey)
      .stream()
      .collect(Collectors.toMap(SongIndexEntry::getSearchToken, SongIndexEntry::getOccurrenceCount));
  }

  @Override
  public Map<String, SearchTokenOccuranceCount> refineFind(String searchToken, Set<String> songKeys) {
    List<SongIndexEntryKey> searchKeys = songKeys.stream().map(s -> new SongIndexEntryKey(searchToken, s))
      .collect(Collectors.toList());


    return StreamSupport.stream(songIndexRepository.findAllById(searchKeys).spliterator(), false)
      .collect(Collectors.toMap(SongIndexEntry::getSongKey, SongIndexEntry::getOccurrenceCount));
  }

  @Override
  public void removeEntries(String songKey) {
    songIndexRepository.deleteBySongKey(songKey);
  }

  @Override
  public void clear() {
    songIndexRepository.deleteAll();
  }
}
