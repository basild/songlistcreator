package com.basildsouza.music.songpresentor.slideshow;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.poi.xslf.usermodel.XMLSlideShow;

import com.basildsouza.music.programguide.Song;

public class SlideShowComparator {
  private static final MultiSongSlideShowSplitter splitter = new MultiSongSlideShowSplitter(new PageNumberParser());
  private static final PoiSlideshowReader reader = new PoiSlideshowReader();
  private static final SlideShowParser parser = new SlideShowParser();
  
  public static void main(String[] args) throws FileNotFoundException, IOException {
    File goldenSource = new File("H:\\Webdrives\\GST - Google Drive\\Hymn Lyrics\\Songs for Renewal.pptx");
    File toCompare = new File("H:\\Webdrives\\GST - Google Drive\\Hymn Lyrics\\Songs selected - Minin Entries - Unparsable.pptx");
    List<Song> goldenSongs = extractSongs(goldenSource);
    List<Song> toCompareSongs = extractSongs(toCompare);
    
    List<String> perfectMatches = new LinkedList<>();
    List<String> imperfectMatches = new LinkedList<>();
    List<String> newSongs = new LinkedList<>();
    for(Song newSong : toCompareSongs){
      boolean songFound = false;
      boolean perfectFound = false;
      for(Song goldenSong : goldenSongs){
        if(!newSong.getTitle().equals(goldenSong.getTitle())){
          continue;
        }
        songFound = true;
        if(newSong.getBody().equals(goldenSong.getBody())){
          perfectMatches.add(goldenSong.getTitle());
          perfectFound = true;
          break;
        }
      }
      if(!songFound){
        newSongs.add(newSong.getTitle());
        continue;
      }
      if(!perfectFound){
        imperfectMatches.add(newSong.getTitle());
      }
    }
    
    System.out.println("Perfect Matches: " + perfectMatches);
    System.out.println("Partial Matches: " + imperfectMatches);
    System.out.println("New Songs: " + newSongs);
  }

  private static List<Song> extractSongs(File goldenSource)
      throws FileNotFoundException, IOException {
    XMLSlideShow slideShow = reader.readSlide(goldenSource);
    List<XMLSlideShow> splitPpts = splitter.splitPpts(slideShow);
    List<Song> songs = splitPpts.stream().map(parser::parseSlide).collect(Collectors.toList());
    return songs;
  }
  
  
}
