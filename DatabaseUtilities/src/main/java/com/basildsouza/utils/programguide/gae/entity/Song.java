package com.basildsouza.utils.programguide.gae.entity;

public class Song {
	private Long songID;
	
	private String title;
	
	private String lyrics;
	
	private String type;
	
	private String key;
	
	private String timing;

	private String rhythm;

	private String referenceURL;

	public Long getSongID() {
		return songID;
	}

	public void setSongID(Long songID) {
		this.songID = songID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLyrics() {
		return lyrics;
	}

	public void setLyrics(String lyrics) {
		this.lyrics = lyrics;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getTiming() {
		return timing;
	}

	public void setTiming(String timing) {
		this.timing = timing;
	}

	public String getReferenceURL() {
		return referenceURL;
	}

	public void setReferenceURL(String referenceURL) {
		this.referenceURL = referenceURL;
	}
	
	public String getRhythm() {
		return rhythm;
	}
	
	public void setRhythm(String rhythm) {
		this.rhythm = rhythm;
	}

	@Override
	public String toString() {
		return "Song [songID=" + songID + ", title=" + title + ", lyrics="
				+ lyrics + ", type=" + type + ", key=" + key + ", timing="
				+ timing + ",rhythm=" + rhythm + ", referenceURL=" + referenceURL + "]";
	}
}