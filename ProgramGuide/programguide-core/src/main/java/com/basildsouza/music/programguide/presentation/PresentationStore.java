package com.basildsouza.music.programguide.presentation;

public interface PresentationStore {
  void save(String presentationKey, byte[] presentation);

  void delete(String presentationKey);

  byte[] get(String presentationKey);


}
