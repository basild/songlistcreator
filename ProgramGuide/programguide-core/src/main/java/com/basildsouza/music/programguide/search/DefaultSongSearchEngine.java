package com.basildsouza.music.programguide.search;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class DefaultSongSearchEngine implements SongSearchEngine {
  private final SongIndex songIndex;
  private final CommonWordExcluder excluder;
  private final LyricsTokenizer tokenizer;

  @Override
  public List<SongSearchResult> findSong(String searchEntry) {
    List<String> searchTokens = tokenizer.tokenize(searchEntry)
      .filter(excluder::isIncluded)
      .collect(Collectors.toList());

    //TODO: This can be made much more efficient with the use of "refineFind" instead
    Map<String, SongSearchWeighter> accumulatedResults = new HashMap<>();
    for (String searchToken : searchTokens) {
      // TODO: Use this after test cases are written and then comment out everything until accumulatedResults.isEmpty
      Map<String, SearchTokenOccuranceCount> currResults = accumulatedResults.isEmpty() ? songIndex.find(searchToken) : songIndex.refineFind(searchToken, accumulatedResults.keySet());

      if (accumulatedResults.isEmpty()) {
        accumulatedResults = copyOverOccurrences(currResults);
        continue;
      }

      updateRanking(accumulatedResults, currResults);

      if (accumulatedResults.isEmpty()) {
        break;
      }
    }
    return buildResultList(accumulatedResults);
  }

  private void updateRanking(Map<String, SongSearchWeighter> accumulatedResults, Map<String, SearchTokenOccuranceCount> currResults) {
    accumulatedResults.keySet().retainAll(currResults.keySet());

    for (Map.Entry<String, SongSearchWeighter> foundEntry : accumulatedResults.entrySet()) {
      SearchTokenOccuranceCount occurrenceCount = currResults.get(foundEntry.getKey());
      foundEntry.getValue().weighOverallOccurrence(occurrenceCount.getTotalCount())
        .weighTitleOccurrence(occurrenceCount.getTitleCount());
    }
  }

  private List<SongSearchResult> buildResultList(Map<String, SongSearchWeighter> foundResults) {
    List<SongSearchResult> songSearchResults =
      foundResults.values()
        .stream()
        .map(s -> new SongSearchResult(s.getSongKey(), s.getTotalWeight()))
        .collect(Collectors.toList());

    songSearchResults.sort((o1, o2) -> Integer.compare(o1.getWeight(), o2.getWeight()) * -1);
    return songSearchResults;
  }

  private Map<String, SongSearchWeighter> copyOverOccurrences(Map<String, SearchTokenOccuranceCount> currentOccurrences) {

    return currentOccurrences.entrySet()
      .stream()
      .map(e -> new SongSearchWeighter(e.getKey()).weighOverallOccurrence(e.getValue().getTotalCount())
        .weighTitleOccurrence(e.getValue().getTitleCount()))
      .collect(Collectors.toMap(SongSearchWeighter::getSongKey, Function.identity()));
  }
}