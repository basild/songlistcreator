package com.basildsouza.music.programguide.index;

import java.util.List;

public interface CommonWordExcluder {
  public List<String> removeCommonWords(List<String> individualWords);
}
