package com.basildsouza.music.programguide.search;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Embeddable;
import java.io.Serializable;


@Embeddable
@AllArgsConstructor
@Getter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class SearchTokenOccuranceCount implements Serializable {
  private int titleCount;
  private int totalCount;
}