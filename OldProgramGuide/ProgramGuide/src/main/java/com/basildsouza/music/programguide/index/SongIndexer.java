package com.basildsouza.music.programguide.index;

import com.basildsouza.music.programguide.Song;
import com.basildsouza.music.programguide.SongEntry;

public interface SongIndexer {
  SongEntry addSong(Song song);

  void removeSong(SongEntry songEntry);
}
