package com.basildsouza.music.programguide;

import com.basildsouza.music.programguide.index.SongIndexer;
import com.basildsouza.music.programguide.store.SongStore;

public class DefaultSongCatalog implements SongCatalog {
  private final SongStore songStore;
  private final SongIndexer songIndexer;

  public DefaultSongCatalog(SongStore songStore, SongIndexer songIndexer) {
    this.songStore = songStore;
    this.songIndexer = songIndexer;
  }

  public Song getSong(SongKey songKey) {
    return getSongEntry(songKey).getSong();
  }

  public SongEntry getSongEntry(SongKey songKey) {
    SongEntry songEntry = songStore.getSongEntry(songKey);
    if (songEntry == null) {
      throw new IllegalStateException("Unable to find entry for key: "
          + songKey);
    }
    return songEntry;
  }

  public SongKey addSong(Song song) {
    SongEntry songEntry = songIndexer.addSong(song);
    songStore.addSongEntry(songEntry);
    return songEntry.getSongKey();
  }

  public void removeSong(SongKey songKey) {
    SongEntry songEntry = songStore.getSongEntry(songKey);
    songIndexer.removeSong(songEntry);
  }
}
