package com.basildsouza.music.programguide.index;

import java.util.List;

public interface SongSearcher {

  List<SongOccurance> findSong(SearchEntry searchEntry);
  
}
