package com.basildsouza.music.programguide;

public interface SongProvider {
  Song getSong(SongKey songKey);

  SongEntry getSongEntry(SongKey songKey);
}
