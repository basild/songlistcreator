package com.basildsouza.music.programguide.search;

import lombok.Data;

@Data
public class SongSearchResult {
  private final String songKey;
  private final int weight;
}
