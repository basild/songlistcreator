package com.basildsouza.music.programguide.search.integration;

import com.basildsouza.music.programguide.search.SongIndexer;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = SearchEngineIntegrationConfig.class)
@ActiveProfiles("Persisted")
@DataJpaTest
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SongIndexSteps {
//  private final SearchSongStepDataHolder songStepDataHolder;
  private final SongIndexer songIndexer;

//  @Given("^song with title '(.*?)'$")
//  public void songWithTitle(String songTitle) {
//    songStepDataHolder.getSingleSearchSongBuilder().title(songTitle);
//  }
//
//  @And("^song with body '(.*?)'$")
//  public void songWithBody(String songBody) {
//    songStepDataHolder.getSingleSearchSongBuilder().body(songBody);
//  }
//
//  @And("^song indexed$")
//  public void songIndexed() {
//    SingleSongSearchData song = songStepDataHolder.getSingleSearchSongBuilder().build();
//    songIndexer.addSong(song.getSongKey(), song.getTitle(), song.getBody());
//  }

  @Given("^song indexed with key '(.*?)' and title '(.*?)' and body '(.*?)'$")
  public void songIndexedWithTitleAndBody(String songKey, String title, String body) {
    songIndexer.addSong(songKey, title, body);
  }

  @And("^song with key '(.*?)' is removed from index$")
  public void songWithIdIsRemoved(String songKey) {
    songIndexer.removeSong(songKey);
  }

  @And("^song re-indexed with key '(.*?)' and title '(.*?)' and body '(.*?)'$")
  public void songReIndexedWithKeyAndTitleTERMAndBodyAnyContent(String songKey, String title, String body) throws Throwable {
    songIndexer.editSong(songKey, title, body);
  }
}
