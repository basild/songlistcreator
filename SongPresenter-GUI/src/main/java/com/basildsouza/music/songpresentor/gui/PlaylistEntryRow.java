package com.basildsouza.music.songpresentor.gui;

import com.basildsouza.music.programguide.SongKey;

class PlaylistEntryRow {
  private final SongKey songKey;
  private final String title;
  private final int numPages;
  
  public PlaylistEntryRow(SongKey songKey, String title, int numPages) {
    this.songKey = songKey;
    this.title = title;
    this.numPages = numPages;
  }
  
  public int getNumPages() {
    return numPages;
  }
  
  public SongKey getSongKey() {
    return songKey;
  }
  
  public String getTitle() {
    return title;
  }
  
}