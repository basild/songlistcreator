package com.basildsouza.music.songpresentor.slideshow;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.poi.xslf.usermodel.XMLSlideShow;

import com.basildsouza.music.programguide.Song;
import com.basildsouza.music.programguide.SongEntry;
import com.basildsouza.music.programguide.index.SongIndexer;

public class MultiSongSlideShowImporter implements SlideShowImporter {
  private final SlideShowReader slideshowReader;
  private final SlideShowStore slideShowStore;
  private final SlideShowParser slideShowParser;
  private final SongIndexer songIndexer;
  private final MultiSongSlideShowSplitter slideShowSplitter;

  public MultiSongSlideShowImporter(SlideShowReader slideshowReader,
                                    SongIndexer songIndexer, 
                                    SlideShowParser slideShowParser,
                                    SlideShowStore slideShowStore,
                                    MultiSongSlideShowSplitter slideShowSplitter) {
    this.slideshowReader = slideshowReader;
    this.songIndexer = songIndexer;
    this.slideShowParser = slideShowParser;
    this.slideShowStore = slideShowStore;
    this.slideShowSplitter = slideShowSplitter;
  }

  public void importSlides(File ppt) throws IOException {
    XMLSlideShow multiSongPpt = slideshowReader.readSlide(ppt);
    List<XMLSlideShow> splitPpts = slideShowSplitter.splitPpts(multiSongPpt);
    for(XMLSlideShow slideShow : splitPpts){
      Song song = slideShowParser.parseSlide(slideShow);
      SongEntry songEntry = songIndexer.addSong(song);
      slideShowStore.addSong(songEntry, slideShow);
    }
  }
}
