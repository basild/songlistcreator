package com.basildsouza.music.programguide.index;

import com.basildsouza.music.programguide.SongKey;

public interface SongKeyGenerator {
  SongKey getNextKey();
}
