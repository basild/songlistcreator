package com.basildsouza.music.songpresentor.slideshow;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xslf.usermodel.XMLSlideShow;

import com.basildsouza.music.programguide.Song;
import com.basildsouza.music.programguide.SongEntry;
import com.basildsouza.music.programguide.index.SongIndexer;

public class IndividualSlideShowImporter implements SlideShowImporter {
  
  
  private final SongIndexer songIndexer;
  private final SlideShowParser slideShowParser;
  private final SlideShowStore slideShowStore;
  private final SlideShowReader slideshowReader;

  public IndividualSlideShowImporter(SlideShowReader slideshowReader,
                           SongIndexer songIndexer, 
                           SlideShowParser slideShowParser, 
                           SlideShowStore slideShowStore) {
    this.slideshowReader = slideshowReader;
    this.songIndexer = songIndexer;
    this.slideShowParser = slideShowParser;
    this.slideShowStore = slideShowStore;
  }
  
  public void importSlides(File pptDir) throws FileNotFoundException, IOException {
    for(File pptFile : pptDir.listFiles()){
      XMLSlideShow slideShow = slideshowReader.readSlide(pptFile);
      Song song = slideShowParser.parseSlide(slideShow);
      SongEntry songEntry = songIndexer.addSong(song);
      slideShowStore.addSong(songEntry, slideShow);
    }
  }
}
