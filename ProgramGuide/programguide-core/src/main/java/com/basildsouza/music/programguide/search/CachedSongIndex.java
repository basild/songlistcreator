package com.basildsouza.music.programguide.search;

import com.basildsouza.music.programguide.manage.Clearable;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Profile("Cached")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CachedSongIndex implements SongIndex, Clearable {
  private final SongIndex fallbackSongIndex;

  private final Map<String, Map<String, SearchTokenOccuranceCount>> occurrences = new HashMap<>();

  @Override
  public void addEntry(String searchToken, String songKey, int titleCount, int totalCount) {
    fallbackSongIndex.addEntry(searchToken, songKey, titleCount, totalCount);

    findInternally(searchToken).put(songKey, new SearchTokenOccuranceCount(titleCount, totalCount));
  }

  @Override
  public Map<String, SearchTokenOccuranceCount> find(String searchToken) {
    return Collections.unmodifiableMap(findInternally(searchToken));
  }

  @Override
  public Map<String, SearchTokenOccuranceCount> refineFind(String searchToken, Set<String> songKeys) {
    return findInternally(searchToken).entrySet()
      .stream()
      .filter(e -> songKeys.contains(e.getKey()))
      .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

  }

  @Override
  public Map<String, SearchTokenOccuranceCount> findBySongKey(String songKey) {
    return fallbackSongIndex.findBySongKey(songKey);
  }

  private Map<String, SearchTokenOccuranceCount> findInternally(String searchToken) {
    occurrences.computeIfAbsent(searchToken, fallbackSongIndex::find);
    return occurrences.get(searchToken);
  }

  @Override
  public void removeEntries(String songKey) {
    fallbackSongIndex.findBySongKey(songKey)
      .keySet()
      .stream()
      .map(occurrences::get)
      .filter(Objects::nonNull)
      .forEach(m -> m.remove(songKey));
    fallbackSongIndex.removeEntries(songKey);
  }

  @Override
  public void clear() {
    occurrences.clear();
  }
}
