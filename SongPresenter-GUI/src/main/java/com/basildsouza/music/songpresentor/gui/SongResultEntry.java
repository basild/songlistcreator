package com.basildsouza.music.songpresentor.gui;

import com.basildsouza.music.programguide.SongKey;

class SongResultEntry {
  private final SongKey songKey;
  private final String title;
  
  public SongResultEntry(SongKey songKey, String title) {
    super();
    this.songKey = songKey;
    this.title = title;
  }
  
  public SongKey getSongKey() {
    return songKey;
  }
  
  public String getTitle() {
    return title;
  }

  @Override
  public String toString() {
    return getTitle();
  }
}