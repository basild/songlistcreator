package com.basildsouza.music.programguide.store;

import com.basildsouza.music.programguide.SongEntry;
import com.basildsouza.music.programguide.SongKey;

public interface SongStore {
  public SongEntry getSongEntry(SongKey songKey);

  public void addSongEntry(SongEntry songEntry);
}
